defmodule PhoenixComponents do
  require Monad.Error, as: Error

  # Convierte lista a map y devuelve %{key => value}
  def convert_to_map(options_list), do: Enum.into(options_list, %{}) |> Map.new(fn {key, val} -> {val, key} end)

  # Ignorar map donde el valor este vacio o nulo
  def clean_map(%{} = map), do: for {k,v} <- map, not(v == "" or v == nil), into: [], do: {k, clean_map(v)}
  def clean_map(v), do: v

  # Ignora key del map
  def ignore_keys(%{} = map, keys), do: Enum.reject(map, fn {k, _} -> String.contains? k, keys end) |> Enum.into(%{})

  # Formatea la fecha en dd/mm/yyyy
  def formatdate(date), do: "#{date.day}/#{date.month}/#{date.year}"

  # Extrae la fecha desde iso8601
  def convert_date(iso8601), do: NaiveDateTime.from_iso8601(iso8601) |> extractdate

  # Extrae la fecha
  defp extractdate({:ok, date}), do: date
  defp extractdate(_), do: DateTime.utc_now()

  # Rutas a partir de la app configurada
  def routes, do: web_module() |> String.to_atom |> Module.concat(Router.Helpers)

  # Nota: Debe tener configurado en APP_NAME la app
  defp web_module, do: "#{System.fetch_env!("APP_NAME") |> appname |> Macro.camelize}Web"

  # Funcion aux para el nombre de la app
  defp appname(app) when is_atom(app), do: app |> Atom.to_string
  defp appname(app), do: app

  def call(param, name) do
    try do
      :pg.get_members(name)
      |> Enum.random()
      |> GenServer.call(param)
      |> to_monad()
    catch
      _what, _value -> :not_found |> Error.fail()
    end
  end

  defp to_monad(r) when is_tuple(r), do: r
  defp to_monad(r), do: r |> Error.return()
end
