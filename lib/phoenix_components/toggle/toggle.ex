defmodule PhoenixComponents.Toggle do
  use Surface.LiveComponent

  prop enabled, :boolean, default: false
  prop name, :string
  prop class, :css_class
  prop primary, :string, default: "primary-200"
  prop options, :tuple, required: true

  def handle_event("toggled", _params, socket) do
    {:noreply, socket |> update(:enabled, &(!&1))}
  end

  defp get_value(true, options), do: elem(options, 0)
  defp get_value(false, options), do: elem(options, 1)
end
