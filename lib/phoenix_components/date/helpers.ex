defmodule PhoenixComponents.DatePicker.Helpers do
  require Logger
  use Timex
  alias Timex.Timezone

  def week_start_at(), do: :mon

  def one_hundred_years_ago() do
    now()
    |> Timex.shift(months: -1200)
    |> Timex.beginning_of_year()
  end

  def one_hundred_years_after() do
    now()
    |> Timex.shift(months: 1200)
    |> Timex.end_of_year()
  end

  def today?(date, time_zone) do
    time_zone
    |> now()
    |> same_date?(date)
  end

  def get_time_zone(offset) do
    Timezone.name_of(offset)
  end

  def now(timezone_name) do
    now() |> Timezone.convert(timezone_name)
  end

  def now() do
    Timex.now()
  end

  def new_interval(nil, nil) do
    one_hundred_years_ago()
    |> new_interval(one_hundred_years_after())
  end

  def new_interval(nil, until) do
    one_hundred_years_ago()
    |> new_interval(until)
  end

  def new_interval(from, nil) do
    from
    |> new_interval(one_hundred_years_after())
  end

  def new_interval(from, until) do
    Interval.new(from: from, until: until, right_open: false)
    |> case do
      {:error, :invalid_until} ->
        Logger.warn("Definicion no valida para el intervalo. Definiendo valor por defecto.")
        new_interval(from, one_hundred_years_after())
      %Timex.Interval{ from: {:error, :invalid_date}} ->
        Logger.warn("Definicion no valida para el intervalo. Definiendo valor por defecto.")
        new_interval(one_hundred_years_ago(), until)
      %Timex.Interval{} = interval -> interval
    end
  end

  def same_date?(date0, date1),
    do: Map.take(date0, [:year, :month, :day]) == Map.take(date1, [:year, :month, :day])

  def range_in_days(start_date, end_date),
  do: { start_date |> Timex.beginning_of_day(), end_date |> Timex.end_of_day() }

  def range_in_days(date),
    do: { date |> Timex.beginning_of_day(), date |> Timex.end_of_day() }

  def range_in_months(start_date, end_date),
    do: { start_date |> Timex.beginning_of_month(), end_date |> Timex.end_of_month() }

  def range_in_months(date),
    do: { date |> Timex.beginning_of_month(), date |> Timex.end_of_month() }

  def range_in_years(start_date, end_date),
    do: { start_date |> Timex.beginning_of_year(), end_date |> Timex.end_of_year() }

  def range_in_years(date),
    do: { date |> Timex.beginning_of_year(), date |> Timex.end_of_year() }

  def day_names(:mon), do: [1, 2, 3, 4, 5, 6, 7] |> Enum.map(&days_of_week/1)
  def day_names(_), do: [7, 1, 2, 3, 4, 5, 6] |> Enum.map(&days_of_week/1)

  def week_rows(month) do
    first =
      month
      |> Timex.beginning_of_month()
      |> Timex.beginning_of_week(week_start_at())

    last =
      month
      |> Timex.end_of_month()
      |> Timex.end_of_week(week_start_at())

    diff = Timex.diff(last, first, :weeks)

    {new_first, new_last} = complete_interval(month, first, last, diff)

    Interval.new(from: new_first, until: new_last)
    |> Enum.map(& &1)
    |> Enum.chunk_every(7)
  end

  defp complete_interval(month, first, last, 4) do
    diff_first = Timex.diff(month |> Timex.beginning_of_month(), first)
    diff_last = Timex.diff(last, month |> Timex.end_of_month())

    if diff_first <= diff_last do
      {first |> Timex.shift(weeks: -1), last}
    else
      {first, last |> Timex.shift(weeks: 1)}
    end
  end

  defp complete_interval(_month, first, last, 3) do
    {first |> Timex.shift(weeks: -1), last |> Timex.shift(weeks: 1)}
  end

  defp complete_interval(_month, first, last, _diff) do
    {first, last}
  end

  def days_of_week(2), do: "Mar"
  def days_of_week(1), do: "Lun"
  def days_of_week(3), do: "Mie"
  def days_of_week(4), do: "Jue"
  def days_of_week(5), do: "Vie"
  def days_of_week(6), do: "Sab"
  def days_of_week(7), do: "Dom"

  def sp_month(1), do: "Enero"
  def sp_month(2), do: "Febrero"
  def sp_month(3), do: "Marzo"
  def sp_month(4), do: "Abril"
  def sp_month(5), do: "Mayo"
  def sp_month(6), do: "Junio"
  def sp_month(7), do: "Julio"
  def sp_month(8), do: "Agosto"
  def sp_month(9), do: "Septiembre"
  def sp_month(10), do: "Octubre"
  def sp_month(11), do: "Noviembre"
  def sp_month(12), do: "Diciembre"

  def short_month(1), do: "Ene"
  def short_month(2), do: "Feb"
  def short_month(3), do: "Mar"
  def short_month(4), do: "Abr"
  def short_month(5), do: "May"
  def short_month(6), do: "Jun"
  def short_month(7), do: "Jul"
  def short_month(8), do: "Ago"
  def short_month(9), do: "Sep"
  def short_month(10), do: "Oct"
  def short_month(11), do: "Nov"
  def short_month(12), do: "Dic"

  def formatt(nil, _), do: nil
  def formatt("", _), do: nil
  def formatt(_date, nil), do: nil

  def formatt(date, time_zone_offset) when is_integer(time_zone_offset) do
    formatt(date, Timezone.name_of(time_zone_offset))
  end

  def formatt(date, time_zone) do
    date
    |> String.split(" - ")
    |> Enum.map(&Timex.parse(&1, "{ISO:Extended:Z}"))
    |> Enum.map(&to_string_date(&1, time_zone))
    |> Enum.join(" - ")
  end

  defp to_string_date({:ok, datestr}, time_zone) do
    datestr
    |> Timezone.convert(time_zone)
    |> Timex.format!("%d/%m/%Y", :strftime)
  end

  defp to_string_date({:error, reason}, _) do
    Logger.warn("Imposible formatear la fecha. #{inspect(reason)}")
    "Error"
  end

  defmodule Range do
    alias PhoenixComponents.DatePicker.Helpers

    @default [
      today: %{label: "Hoy", amount: 0, in: :days},
      yesterday: %{label: "Ayer", amount: -1, in: :days},
      last_7days: %{label: "Últimos 7 días", amount: -7, in: :days},
      last_30days: %{label: "Últimos 30 días", amount: -30, in: :days},
      this_month: %{label: "Mes actual", amount: 0, in: :months},
      last_month: %{label: "Mes pasado", amount: -1, in: :months},
      this_year: %{label: "Año actual", amount: 0, in: :years}
      # last_year: %{label: "Año pasado", amount: -1, in: :years},
    ]
    @derive {
      Jason.Encoder,
      only: [
        :key,
        :label,
        :value
      ]
    }
    defstruct [:key, :label, interval: nil, value: nil]

    def new(%{label: l, amount: a, in: step} = elem, now) when is_binary(l) and is_integer(a) and is_atom(step) do
      dates(a, step, now)
      |> case do
        nil ->
          Logger.debug("El rango: #{inspect(elem)}, no cumple con la especificacion definida para el componente.")
          nil
        {from, to} ->
          %__MODULE__{
            key: generate_key(a, step),
            label: l,
            interval: Helpers.new_interval(from, to),
            value: new_value(from, to)
            }
      end
    end

    def new(%{label: l, amount: a, in: step} = elem, now) when is_binary(l) and is_binary(a) and is_binary(step),
      do: new(%{elem | amount: String.to_integer(a), in: String.to_existing_atom(step)}, now)

    def new(default_key, now) when is_atom(default_key) do
      @default
      |> Enum.find(fn {k,_} -> k == default_key end)
      |> case do
        nil ->
          Logger.debug("El rango para la llave: #{inspect(default_key)}, no esta definido.")
          nil
        {_, elem} ->
          new(elem, now)
      end
    end

    def new(default_key, now) when is_binary(default_key), do: new(String.to_atom(default_key), now)

    def new(elem, _now) do
      Logger.debug("El rango: #{inspect(elem)}, no cumple con la especificacion definida para el componente.")
      nil
    end

    def custom(label),
      do: %__MODULE__{ key: "custom", label: label}

    def defaults(now) do
      @default
      |> Enum.map(fn {_, elem} -> new(elem, now) end)
    end

    def generate_key(0, :days), do: "today"
    def generate_key(1, :days), do: "tomorrow"
    def generate_key(-1, :days), do: "yesterday"
    def generate_key(n, step) when n == 0, do: "this_#{Atom.to_string(step)}"
    def generate_key(n, step) when n == 1, do: "next_#{Atom.to_string(step)}"
    def generate_key(n, step) when n == -1, do: "last_#{Atom.to_string(step)}"
    def generate_key(n, step) when n > 1, do: "next_#{n}#{Atom.to_string(step)}"
    def generate_key(n, step), do: "last_#{abs(n)}#{Atom.to_string(step)}"

    # Hoy
    defp dates(0, :days, now),
     do: now |> Helpers.range_in_days()

    # Mañana
    defp dates(1, :days, now),
     do: now |> Timex.shift(days: 1) |> Helpers.range_in_days()

    # Ayer
    defp dates(-1, :days, now),
     do: now |> Timex.shift(days: -1) |> Helpers.range_in_days()

    # Proximos n dias
    defp dates(n, :days, now) when n > 1,
     do: now |> Timex.shift(days: 1) |> Helpers.range_in_days(now |> Timex.shift(days: n))

    # Ultimos n dias
    defp dates(n, :days, now),
     do: now |> Timex.shift(days: (n + 1)) |> Helpers.range_in_days(now)

    # Este mes
    defp dates(0, :months, now),
      do: now |> Helpers.range_in_months()

    # Proximo mes
    defp dates(1, :months, now),
      do: now |> Timex.shift(months: 1) |> Helpers.range_in_months()

    # Pasado mes
    defp dates(-1, :months, now),
      do: now |> Timex.shift(months: -1) |> Helpers.range_in_months()

    # Proximos n meses
    defp dates(n, :months, now) when n > 1,
      do: now |> Timex.shift(months: 1) |> Helpers.range_in_months(now |> Timex.shift(months: n))

    # Ultimos n meses
    defp dates(n, :months, now),
      do: now |> Timex.shift(months: (n + 1)) |> Helpers.range_in_months(now)

    # Este año
    defp dates(0, :years, now),
      do: now |> Helpers.range_in_years()
    # Proximo año
    defp dates(1, :years, now),
      do: now |> Timex.shift(years: 1) |> Helpers.range_in_years()
    # Año pasado
    defp dates(-1, :years, now),
      do: now |> Timex.shift(years: -1) |> Helpers.range_in_years()
    # Proximos n años
    defp dates(n, :years, now) when n > 1,
      do: now |> Timex.shift(years: 1) |> Helpers.range_in_years(now |> Timex.shift(years: n))
    # Ultimos n años
    defp dates(n, :years, now),
      do: now |> Timex.shift(years: (n + 1)) |> Helpers.range_in_years(now)

    defp dates(_n, _step, _now),
      do: nil

    defp new_value(start_date, end_date),
      do: "#{Timex.format!(start_date, "{ISO:Extended:Z}")} - #{Timex.format!(end_date, "{ISO:Extended:Z}")}"
  end
end
