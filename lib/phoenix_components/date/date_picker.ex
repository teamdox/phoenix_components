defmodule PhoenixComponents.DatePicker do
  @moduledoc """
  Documentation for DatePicker.
  """
  use Surface.LiveComponent
  use Timex
  require Logger

  alias Timex.Timezone
  alias PhoenixComponents.DatePicker.{Helpers, Helpers.Range, Calendar, OptionRange}
  alias PhoenixComponents.Svg
  alias Surface.Components.Form.HiddenInput

  @tz_default Timezone.name_of(0)

  prop name, :string, required: true
  prop value, :any, required: true
  prop tz_offset, :integer, required: true
  prop placeholder, :string, default: "Seleccione"
  prop left, :boolean, default: false

  prop focused_class, :string, default: "border border-gray-300"
  prop sufix_class, :css_class, default: "blue-600"
  prop sufix_hover_class, :css_class, default: "blue-400"

  prop ranges, :any, default: []
  prop custom_range?, :boolean, default: false
  prop single_picker?, :boolean, default: true
  prop min_date, :datetime, default: nil
  prop max_date, :datetime, default: nil

  data time_zone, :string, default: nil
  data range_selected, :any, default: nil
  data range_options, :any, default: []
  data show_calendar, :boolean, default: false

  #Mes de cada calendario
  data current_month, :datetime, default: Timex.now()
  data right_month, :datetime, default: Timex.now()
  data left_month, :datetime, default: (Timex.now() |> Timex.shift(months: -1))

  #Intervalos en los calendario
  data right_interval, :any, default: nil
  data left_interval, :any, default: nil
  data interval, :any, default: nil

  #Viste de los calendario
  data right_view, :atom, default: :date
  data left_view, :atom, default: :date
  data view, :atom, default: :date

  #Fecha seleccionada en cada calendario
  data current_date, :datetime, default: nil
  data start_date, :datetime, default: nil
  data end_date, :datetime, default: nil


  @default_data %{
    range_selected: nil,
    show_calendar: false,
    right_month: Timex.now(),
    start_date: nil,
    end_date: nil,
    current_month: Timex.now(),
    current_date: nil
  }

  def update(assigns, socket) do
    {:ok,
      socket
        |> assign(assigns)
        |> set_data()
        |> set_time_zone()
        |> configure_interval()
        |> configure_ranges()
        |> update_picker()
    }
  end

  defp set_data(%{assigns: %{value: value, range_selected: range_selected}} = socket)
    when (value == nil or value == "") and range_selected != nil,
    do: socket |> assign(range_selected: nil) |> assign(show_calendar: false)

  defp set_data(socket), do: socket

  defp set_time_zone(%{assigns: %{tz_offset: tz_offset, time_zone: nil}} = socket),
    do: socket |> assign(time_zone: tz_offset |> Helpers.get_time_zone() |> ok_time_zone())

  defp set_time_zone(socket), do: socket

  defp ok_time_zone(time_zone) do
    if Timex.is_valid_timezone?(time_zone), do: time_zone, else: @tz_default
  end

  defp configure_interval(%{assigns: %{min_date: min_date, max_date: max_date, interval: nil}} = socket),
  do: socket |> assign(interval: Helpers.new_interval(min_date, max_date))

  defp configure_interval(socket), do: socket

  defp configure_ranges(%{assigns: %{single_picker?: false}} = socket),
    do: socket |> ranges_definitions()

  defp configure_ranges(socket), do: socket

  defp update_picker(
         %{
           assigns: %{
            single_picker?: true,
            time_zone: time_zone,
            current_month: current_month
           }
         } = socket
       ) do

    socket
    |> assign(:current_month, current_month |> Timezone.convert(time_zone))
  end

  defp update_picker(
         %{
           assigns: %{
            single_picker?: false,
            time_zone: time_zone,
            left_month: left_month,
            right_month: right_month
           }
         } = socket
       ) do
    right_month = right_month |> Timezone.convert(time_zone) |> Timex.beginning_of_month()

    left_month = left_month |> Timezone.convert(time_zone) |> Timex.beginning_of_month()

    socket |> set_values_to_picker(left_month, right_month)
  end

  defp update_picker(socket), do: socket

  def handle_event("clear", _, socket) do
    {:noreply,
     socket
     |> assign(@default_data)
     |> assign(left_month: Helpers.now() |> Timex.shift(months: -1))
     |> update_picker()
     |> update_and_push_event(nil)
    }
  end

  def handle_event("toggle_view", %{"view" => "year"}, socket) do
    {:noreply, socket}
  end

  def handle_event("toggle_view", %{"calendar" => "left", "view" => view}, socket) do
    {:noreply, socket |> assign(left_view: toogle_up_view(view))}
  end

  def handle_event("toggle_view", %{"calendar" => "right", "view" => view}, socket) do
    {:noreply, socket |> assign(right_view: toogle_up_view(view))}
  end

  def handle_event("toggle_view", %{"view" => view}, socket) do
    {:noreply, socket |> assign(view: toogle_up_view(view))}
  end

  def handle_event("prev", %{"calendar" => "left", "view" => view},
    %{assigns: %{ left_month: month, left_interval: interval }} = socket) do

    {:noreply,
      socket
      |> assign(left_month: previous_date(String.to_existing_atom(view), month, interval))
      |> update_intervals()
    }
  end

  def handle_event("prev", %{"calendar" => "right", "view" => view},
    %{assigns:
      %{ right_month: right_month, right_interval: interval}} = socket) do

    {:noreply,
      socket
      |> assign(right_month: previous_date(String.to_existing_atom(view), right_month , interval))
      |> update_intervals()
    }
  end

  def handle_event("prev",  %{"view" => view},
    %{assigns: %{ current_month: month, interval: interval }} = socket) do

    {:noreply,
      socket
      |> assign(current_month: previous_date(String.to_existing_atom(view), month, interval))
    }
  end

  def handle_event("next", %{"calendar" => "left", "view" => view},
    %{assigns: %{ left_month: left_month, left_interval: interval }} = socket) do

    {:noreply,
      socket
      |> assign(left_month: next_date(String.to_existing_atom(view), left_month, interval))
      |> update_intervals()
    }
  end

  def handle_event("next", %{"calendar" => "right", "view" => view},
    %{assigns: %{ right_month: month, right_interval: interval }} = socket) do

    {:noreply,
      socket
      |> assign(right_month: next_date(String.to_existing_atom(view), month, interval))
      |> update_intervals()
    }
  end

  def handle_event("next", %{"view" => view},
    %{assigns: %{ current_month: month, interval: interval }} = socket) do

    {:noreply,
      socket
      |> assign(current_month: next_date(String.to_existing_atom(view), month, interval))
    }
  end

  def handle_event("pick_date", %{"block" => "true"}, socket) do
    {:noreply, socket}
  end

  def handle_event("pick_date", %{"date" => date, "calendar" => "left"}, %{assigns: %{left_view: view}} = socket) do
    case Timex.parse(date, "{ISO:Extended:Z}") do
      {:ok, date} ->
        {:noreply, socket  |> assign(left_month: date) |> assign(left_view: toogle_down_view(view)) }
      _ ->
        Logger.warn("No se reconoce el formato para la fecha #{inspect(date)}")
        {:noreply, socket}
    end
  end

  def handle_event("pick_date", %{"date" => date, "calendar" => "right"}, %{assigns: %{right_view: view}} = socket) do
    case Timex.parse(date, "{ISO:Extended:Z}") do
      {:ok, date} ->
        {:noreply, socket  |> assign(right_month: date) |> assign(right_view: toogle_down_view(view)) }
      _ ->
        Logger.warn("No se reconoce el formato para la fecha #{inspect(date)}")
        {:noreply, socket}
    end
  end

  def handle_event("pick_date", %{"date" => date, "calendar" => _calendar}, %{assigns: %{view: view}} = socket) do
    case Timex.parse(date, "{ISO:Extended:Z}") do
      {:ok, date} ->
        {:noreply, socket  |> assign(current_month: date) |> assign(view: toogle_down_view(view)) }
      _ ->
        Logger.warn("No se reconoce el formato para la fecha #{inspect(date)}")
        {:noreply, socket}
    end
  end

  def handle_event("pick_date", %{"date" => date}, %{assigns: %{single_picker?: true}} = socket) do
    case Timex.parse(date, "%FT%T", :strftime) do
      {:ok, date} ->
        {:noreply,
          socket
          |> assign(current_date: date)
          |> update_and_push_event(Timex.format!(date, "{ISO:Extended:Z}"))
        }
      _ ->
        Logger.warn("No se reconoce el formato para la fecha #{inspect(date)}")
        {:noreply, socket}
    end
  end

  def handle_event("pick_date", %{"date" => date}, socket) do
    case Timex.parse(date, "%FT%T", :strftime) do
      {:ok, date} ->
        {:noreply, socket |> set_custom_range(date)}
      _ ->
        Logger.warn("No se reconoce el formato para la fecha #{inspect(date)}")
        {:noreply, socket}
    end
  end

  def handle_event("range_option_selected", %{"key" => key}, %{assigns: %{range_options: range_options}} = socket) do
    range_selected = range_options |> Enum.find(fn %{key: k} -> k == key end)
    {:noreply,
      socket
      |> assign(:range_selected, range_selected)
      |> show_custom_options()
    }
  end

  def update_and_push_event(socket, value) do
    socket
    |> assign(value: value)
    |> push_event("datepicker_value_change", %{})
  end

  # GET DEFINED RANGE OR CUSTOM
  defp show_custom_options(%{assigns: %{range_selected: %Range{key: "custom"}}} = socket) do
    time_zone = socket.assigns.time_zone
    right_month = Helpers.now(time_zone) |> Timex.beginning_of_month()
    left_month = right_month |> Timex.shift(months: -1) |> Timex.beginning_of_month()

    socket
      |> set_values_to_picker(left_month, right_month)
      |> assign(show_calendar: true)
      |> assign(start_date: nil)
      |> assign(end_date: nil)
  end

  defp show_custom_options(%{assigns: %{range_selected: range}} = socket) do
    socket
    |> assign(show_calendar: false)
    |> update_and_push_event(range.value)
  end

  # PREV DATES
  defp previous_date(:month, current_month, nil), do: Timex.shift(current_month, years: -1)
  defp previous_date(:month, current_month, interval) do
    temporal_date = Timex.shift(current_month, years: -1)
    (temporal_date in interval)
    |> new_date(temporal_date, interval.from)
  end

  defp previous_date(:year, current_month, nil), do: Timex.shift(current_month, years: -10)
  defp previous_date(:year, current_month, interval) do
    temporal_date = Timex.shift(current_month, years: -10)
    (temporal_date in interval)
    |> new_date(temporal_date, interval.from)
  end

  defp previous_date(_mode, current_month, nil), do: Timex.shift(current_month, months: -1)
  defp previous_date(_mode, current_month, interval) do
    temporal_date = Timex.shift(current_month, months: -1)
    (temporal_date in interval)
    |> new_date(temporal_date, interval.from)
  end
  # NEXT DATES
  defp next_date(:month, current_month, nil), do: Timex.shift(current_month, years: 1)
  defp next_date(:month, current_month, interval) do
    temporal_date = Timex.shift(current_month, years: 1)
    (temporal_date in interval)
    |> new_date(temporal_date, interval.until)
  end

  defp next_date(:year, current_month, nil), do: Timex.shift(current_month, years: 10)
  defp next_date(:year, current_month, interval) do
    temporal_date = Timex.shift(current_month, years: 10)
    (temporal_date in interval)
    |> new_date(temporal_date, interval.until)
  end

  defp next_date(_mode, current_month, nil), do: Timex.shift(current_month, months: 1)
  defp next_date(_mode, current_month, interval) do
    temporal_date = Timex.shift(current_month, months: 1)
    (temporal_date in interval)
    |> new_date(temporal_date, interval.until)
  end

  defp new_date(true, date, _interval_date), do: date
  defp new_date(_, _date, interval_date), do: interval_date

  # SELECT A CUSTOM RANGE
  defp set_custom_range(%{assigns: %{start_date: sd, end_date: ed, range_selected: rs}} = socket, date) when sd != nil and ed != nil do
    socket
    |> assign(:start_date, date)
    |> assign(:end_date, nil)
    |> assign(:range_selected, %{rs | interval: nil, value: nil})
  end

  defp set_custom_range(%{assigns: %{start_date: sd, range_selected: rs}} = socket, date) when is_nil(sd) do
    socket
    |> assign(:start_date, date)
    |> assign(:range_selected, %{rs | interval: nil, value: nil})
  end

  defp set_custom_range(%{assigns: %{time_zone: time_zone, start_date: start_date, range_selected: rs}} = socket, date) do
    end_date = date |> Timex.end_of_day() |> Timezone.convert(time_zone)

    if start_date |> Timex.before?(end_date) do

      new_interval = Helpers.new_interval(start_date, end_date)
      value = "#{Timex.format!(start_date, "{ISO:Extended:Z}")} - #{Timex.format!(end_date, "{ISO:Extended:Z}")}"

      socket
      |> assign(:end_date, end_date)
      |> assign(:range_selected, %{rs | interval: new_interval, value: value})
      |> update_and_push_event(value)
    else
      socket |> assign(:start_date, date)
    end
  end


  defp set_values_to_picker(socket, left_month, right_month) do
    socket
    |> assign(left_month: left_month)
    |> assign(right_month: right_month)
    |> update_intervals()
  end

  defp update_intervals(
    %{:assigns =>
      %{
        :left_month => left_month,
        :right_month => right_month,
        :interval => interval
        }} = socket) do

    left_month =  left_month |> Timex.shift(months: 1) |> Timex.to_date() |> Timex.to_datetime() |> Timex.beginning_of_month()
    right_month =  right_month |> Timex.shift(months: -1) |> Timex.to_date() |> Timex.to_datetime() |> Timex.end_of_month()
    socket
    |> assign(left_interval: Helpers.new_interval(interval.from |> Timex.beginning_of_month(), right_month))
    |> assign(right_interval:  Helpers.new_interval(left_month, interval.until))
  end


  defp update_intervals(socket), do: socket

  def default_ranges(time_zone), do: Range.defaults(Helpers.now(time_zone))

  defp custom_range(), do: [Range.custom("Personalizado")]

  # Definir rangos por defecto sin el rango custom
  def ranges_definitions(
        %{assigns: %{time_zone: time_zone, ranges: [], custom_range?: false}} = socket
      ) do
    socket |> assign(range_options: default_ranges(time_zone))
  end
  # Definir rangos por defecto con el rango custom
  def ranges_definitions(%{assigns: %{time_zone: time_zone, ranges: []}} = socket) do
    socket |> assign(range_options: default_ranges(time_zone) ++ custom_range())
  end
  # Construir rangos segun configuracion del componente
  def ranges_definitions(
        %{assigns: %{time_zone: time_zone, ranges: ranges, custom_range?: custom_range}} = socket
      ) do
    socket |> assign(range_options: build_ranges(ranges, time_zone, custom_range))
  end
  # Definir rangos por defecto con el rango custom
  def ranges_definitions(%{assigns: %{time_zone: time_zone}} = socket) do
    Logger.debug("La definición de rangos no se pudo determinar. La opciones de rango toman los valores por defecto.")
    socket |> assign(range_options: default_ranges(time_zone) ++ custom_range())
  end

  defp build_ranges(ranges, time_zone, custom_range) when is_list(ranges) do
    ranges
    |> Enum.map(&(Range.new(&1, Helpers.now(time_zone))))
    |> Enum.reject(&nil_range/1)
    |> case do
      [] ->
        Logger.debug("La definición de rangos no se pudo determinar. La opciones de rango toman los valores por defecto.
        #{inspect(ranges)}"
      )
        default_ranges(time_zone)

      r ->
        #Por cuestiones de dinsenno solo tomamos 7 rangos de los que se especifiquen
        r |> Enum.take(7) |> add_custom_range(custom_range)
    end
  end

  defp build_ranges(ranges, time_zone, _custom_range) do
    Logger.debug(
      "La declaracion de rangos, no cumple con la especificacion definida para el componente.
      #{inspect(ranges)}"
    )

    default_ranges(time_zone)
  end

  defp nil_range(nil), do: true
  defp nil_range(_), do: false

  defp add_custom_range(r, true), do: r ++ custom_range()
  defp add_custom_range(r, _), do: r

  defp toogle_up_view("date"), do: :month
  defp toogle_up_view("month"), do: :year
  defp toogle_up_view(_), do: :date

  defp toogle_down_view(:month), do: :date
  defp toogle_down_view(:year), do: :month
  defp toogle_down_view(_), do: :date


  defp show_clear_opt(nil), do: false
  defp show_clear_opt(value) when is_binary(value), do: String.trim(value) != ""
  defp show_clear_opt(value) when is_map(value), do: value != Map.new

end
