defmodule PhoenixComponents.MonthYear do
  use Surface.Component
  alias PhoenixComponents.DatePicker.Helpers

  prop date, :datetime, required: true
  prop current_date, :datetime, required: true
  prop calendar, :string, default: "current"
  prop calendar_view, :atom, required: true
  prop interval, :any, required: true

  def update(assigns, socket) do
    {
      :ok,
      socket
      |> assign(assigns)
      |> assign(:column_class, column_class(assigns))
    }
  end

  defp column_class(%{calendar_view: view} = assigns) do
    cond do
      current_month_or_year?(assigns) ->
        "border-dashed border-gray-600 hover:bg-gray-200 cursor-pointer"

      out_of_interval?(assigns) ->
        "border-transparent text-gray-300 cursor-not-allowed line-through"
      true ->
        "border-transparent text-black bg-white hover:bg-gray-200 cursor-pointer"
    end
    |> x_padding(view)
  end

  defp column_class(_assigns),
    do: "border-transparent text-black bg-white hover:bg-gray-200 cursor-pointer"

  defp out_of_interval?(%{interval: nil}), do: false
  defp out_of_interval?(%{interval: interval, date: date}), do: date not in interval
  defp out_of_interval?(_), do: false

  defp current_month_or_year?(%{:calendar_view => :month}=assigns) do
    Map.take(assigns.date, [:year, :month]) == Map.take(assigns.current_date, [:year, :month])
  end

  defp current_month_or_year?(assigns) do
    Map.take(assigns.date, [:year]) == Map.take(assigns.current_date, [:year])
  end

  defp get_value(date, :month),
    do: date |> Timex.format!("%b", :strftime) |> Timex.month_to_num() |> Helpers.short_month()

  defp get_value(date, _),
    do: Timex.format!(date, "%Y", :strftime)

  defp x_padding(clazz, :month), do: "#{clazz} px-8"

  defp x_padding(clazz, _), do: "#{clazz} px-7"
end
