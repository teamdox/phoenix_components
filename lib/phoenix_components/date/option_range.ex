defmodule PhoenixComponents.DatePicker.OptionRange do
  use Surface.Component
  prop range, :any, required: true
  prop selected?, :boolean, required: true
  prop click, :event, required: true
  slot default


  defp get_range_class(false, _, hover_class), do: "hover:bg-#{hover_class}"

  defp get_range_class(_, class, _),
    do: "text-white bg-#{class} hover:bg-#{class} border-#{class} hover:border-#{class}"

end
