let DatePickerChange = {
  mounted() {
    this.handleEvent("datepicker_value_change", () => {
      this.el.querySelector('input').dispatchEvent(new Event("input", {bubbles: true}))
    })
  }
};

export {DatePickerChange};
