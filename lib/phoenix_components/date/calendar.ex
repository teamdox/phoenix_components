defmodule PhoenixComponents.DatePicker.Calendar do
  use Surface.Component
  use Timex
  alias PhoenixComponents.DatePicker.Helpers
  alias PhoenixComponents.{Day, MonthYear}
  alias PhoenixComponents.Svg

  prop month, :datetime, required: true
  prop calendar, :string, default: "current"
  prop interval, :any, default: nil
  prop range_selected, :any, default: nil
  prop view, :atom

  @week_start_at :mon
  def mount(socket) do
    {:ok,
      socket
      |> assign(day_names: Helpers.day_names(@week_start_at))
    }
  end

  def update(assigns, socket) do
    {:ok,
      socket
      |> assign(assigns)
      |> configure_weeks()
    }
  end

  defp configure_weeks(%{:assigns => %{:month => month}} = socket) do
    socket
      |> assign(week_rows: Helpers.week_rows(month))
  end

  defp configure_weeks(socket) do
    socket
      |> assign(week_rows: [])
  end

  defp get_header_title(date, calendar_mode) do
    case calendar_mode do
      :month ->
        Timex.format!(date, "%Y", :strftime)

      :year ->
        range_years = create_10_years_range(date, 0, 9) |> Enum.to_list()
        "#{List.first(range_years)} - #{List.last(range_years)}"

      _ ->
        month = Timex.format!(date, "%B", :strftime) |> Timex.month_to_num() |> Helpers.sp_month()
        year = Timex.format!(date, "%Y", :strftime)
        "#{month} #{year}"
    end
  end

  defp create_10_years_range(date, minus_offset, plus_offset) do
    current_year = date |> Map.take([:year]) |> Map.get(:year)
    temporal_rem = current_year |> rem(10)
    (current_year - temporal_rem - minus_offset)..(current_year + plus_offset - temporal_rem)
  end

  defp generate_id_calendar(parent_id, calendar, date) do
    date_format = date |> Timex.format!("%m/%d/%Y", :strftime)

    case calendar do
      "left" -> "#{parent_id}_left_month-#{date_format}"
      _ -> "#{parent_id}_right_month-#{date_format}"
    end
  end

  defp months_or_years(:month, date) do
    current_month = date |> Map.take([:month]) |> Map.get(:month)
    1..12
    |> Enum.map(&Timex.shift(date, months: &1 - current_month))
    |> Enum.chunk_every(3)
  end

  defp months_or_years(_, date) do
    current_year = date |> Map.take([:year]) |> Map.get(:year)

    create_10_years_range(date, 1, 10)
    |> Enum.map(&Timex.shift(date, years: &1 - current_year))
    # |> Enum.map(&Timex.shift(date, years: &1 - current_year) |> Timex.beginning_of_year())
    |> Enum.chunk_every(3)
  end
end
