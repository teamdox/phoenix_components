defmodule PhoenixComponents.NodeDetail do
  use Surface.Component

  prop node, :any, required: true

  slot actions
  slot preview
  slot detail
end
