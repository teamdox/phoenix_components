defmodule PhoenixComponents.NodeDetail.Detail.Header do
  use Surface.Component, slot: "header"
  slot default
end
