defmodule PhoenixComponents.NodeDetail.Detail do
  use Surface.Component, slot: "detail"
  slot actions
  slot header
  slot default
end
