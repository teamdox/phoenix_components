defmodule PhoenixComponents.Node do
  use Surface.Component
  use Phoenix.HTML

  prop node, :any, required: true
  prop parents, :fun
  prop children, :fun
  prop both, :fun

  slot default

  def to_html(map, func_all, func_parent \\ & &1, func_children \\ & &1) do
    map = cond do
      is_map(map) and Map.has_key?(map, :__struct__) -> clean_data(map)
      is_tuple(map) -> [map]
      true -> map
    end

    Enum.reduce(apply_fun(func_parent).(map), [], &apply_html(&1, &2, func_all, func_children))
  end

  defp apply_html({k, v}, acc, func_all, func_children) do
    cond do
      v in ["", nil] -> acc
      is_map(v) and Map.has_key?(v, :__struct__) -> [ body_content(k, v, func_all) | acc]
      is_map(v) and Enum.empty?(v) -> acc
      is_map(v) -> [ root_content(k, v, func_children, func_all) | acc ]
      is_list(v) and Enum.all?(v, &is_binary/1) -> [ body_content(k, Enum.join(v, ", "), func_all) | acc]
      is_list(v) -> [ middle_content(k, v, func_children, func_all) | acc ]
      true -> [ body_content(k, v, func_all) | acc]
    end
  end

  defp root_content(k, v, func_children, func_all) do
    content_tag(:div,
      [ content_tag(:h3, apply_fun(func_all).(k), class: "text-md leading-6 font-medium text-gray-900"),
        content_tag(:div, to_html(apply_fun(func_children).(v), func_all), class: "text-sm ml-2 mt-2 space-y-6 sm:space-y-0 border-gray-300 border-t")
      ],
      class: "mt-2 divide-y divide-gray-300"
    )
  end

  defp middle_content(k, v, func_children, func_all) do
    content_tag(:div,
      [ content_tag(:h3, apply_fun(func_all).(k) |> capitalize, class: "text-md leading-6 font-medium text-gray-900")
      | Enum.map(v, &middle_content(&1, func_children, func_all))
      ]
    )
  end

  defp middle_content(v, func_children, func_all) do
    content_tag(:div,
      [ #content_tag(:h4, "##{k}", class: "pl-2 leading-6 font-medium text-gray-900"),
        content_tag(:div, to_html(apply_fun(func_children).(v), func_all), class: "text-sm ml-2 mt-3 space-y-6 sm:space-y-0 border-gray-300 border-t"),
      ],
      class: "mt-2 divide-y divide-gray-300"
    )
  end

  defp body_content(k, v, func_all) do
    content_tag(:div,
      [ content_tag(:div, apply_fun(func_all).(k) |> capitalize, class: "p-1 font-medium text-gray-700 break-words"),
        content_tag(:div, apply_fun(func_all).(v), class: "p-1 pl-8 sm:col-span-2")
      ],
      class: "border-b divide-x divide-gray-300 sm:grid sm:grid-cols-3"
    )
  end

  # Crea/Agrupa elementos de un mapa
  def new_map(map, %{"fields" => fields, "put" => parent}) do
    map = map |> Map.new
    data_updated = Enum.map(fields, &group(&1, map)) |> not_empty |> Map.new
    case getin!(map, parent, nil) do
      nil -> map
      _data -> update_in(map, parent, &Enum.into(&1, data_updated))
    end
  end

  def new_map(map, %{"fields" => fields}), do: Enum.map(fields, &group(&1, map))
  def new_map(map, _ignore), do: map

  def group({title, %{"parent" => parent, "children" => children}}, map) do
    data = getin!(map, parent, [])
    { title,
      cond do
        is_map(data) -> [Map.take(data, children) |> sort(children) ]
        is_list(data) -> Enum.map(data, &Map.take(&1, children) |> sort(children))
      end
    }
  end

  def group({title, list}, map), do:
    { title,
      [ list
        |> Enum.map(& {List.last(&1), getin!(map, &1, nil)})
        #|> Map.new
      ]
    }

  # Ordenar bloques
  # Nota: Recomendable aplicar al final
  def sort(map, list) when is_list(list), do: Enum.sort_by(map, &sort(list, &1), :desc) #|> Map.new
  def sort(map, {key, _value}), do: Enum.find_index(map, &Kernel.==(&1, key))
  def sort(map, _ignore), do: map

  # Eliminar metadatos
  def delete(_map), do: []
  def delete(map, list) when is_list(list) and is_list(map), do: Enum.reject(map, &search(&1, list))# |> Map.new
  def delete(map, list) when is_list(list), do: Map.to_list(map) |> Enum.reject(&search(&1, list))# |> Map.new
  def delete(map, _ignore), do: map
  def search({key, _value}, list), do: key in list

  # Obtener parametros
  def take_group(map, list) when is_list(list), do: Enum.map(list, &take_tuple(map, &1)) |> Enum.concat |> Map.new
  def take_group(map, _ignore), do: map
  def take_tuple(map, [head | tail]) when is_list(head), do: Map.take(getin!(map, head, %{}), tail)
  def take_tuple(map, _ignore), do: map

  # Traduce
  def translet(k, nil), do: k
  def translet(k, keys) when is_binary(k), do: Map.get(keys, k, String.replace(k, "_", " "))
  def translet(k, keys), do: translet(k |> to_string, keys)

  # Format date
  def apply_by_date(date), do: Timex.is_valid?(date) |> apply_by_date(date)
  def apply_by_date({:ok, date}, _value), do: Timex.format!(date, "%d/%m/%Y", :strftime)
  def apply_by_date({:error, _date}, value), do: value
  def apply_by_date(true, date), do: apply_by_date({:ok, date}, nil)
  def apply_by_date(_ignore, date) when is_binary(date), do: String.upcase(date || "") |> NaiveDateTime.from_iso8601 |> apply_by_date(date)
  def apply_by_date(_ignore, value), do: value

  def capitalize(value) when is_binary(value), do: value |> String.capitalize
  def capitalize(value), do: value

  # Registrar tipo_persona
  def updated_data(map, item, tuple, func) do
    cond do
      nil in [map, item, tuple, func] -> []
      is_list(item) -> Enum.reduce(item, %{}, &updating(&1, &2, map, tuple, func))
      true -> map
    end
  end

  # Actualiza cada elemento de la lista
  def updating(c, acc, map, tuple, func) do
    map = Map.merge(map, acc)
    case getin!(map, c, nil) do
      nil -> map
      _data -> update_in(map, c, &map_type(&1, tuple, func))
    end
  end

  # Registra tipo_persona a partir del rut
  def map_type(items, {title, key}, func) when is_list(items), do: Enum.map(items, &put_tuple(&1, title, key, func))
  def map_type(items, _tuple, _func), do: items
  #def put_tuple(nil, list, _title, _func), do: list
  # Agregar tupla a la lista
  def put_tuple({_key, value}, list, title, func), do: list ++ [{title, apply_fun(func).(value)}]
  # Buscar tupla que coincida con key
  def put_tuple(list, title, key, func), do: Enum.find(list, &search(&1, [key])) |> put_tuple(list, title, func)

  def clean_data(map) do
    Map.delete(map, :__struct__)
    |> Map.delete(:__meta__)
    |> Enum.filter(&elem(&1, 1))
    |> Enum.map(&{Atom.to_string(elem(&1, 0)), elem(&1, 1)} )
    #|> Enum.into(%{})
  end

  def getin(map, parent, default) do
    try do
      {:ok, get_in(map, parent)}
    catch
      _what, _value -> {:ok, default}
    end
  end

  def getin!(map, parent, default) do
    case getin(map, parent, default) do
      {:ok, nil} -> default
      {:ok, data} -> data
      {:error, _error} -> default
    end
  end

  # Filtrar solo listas con elementos
  def not_empty(enum, compare \\ []), do: Enum.filter(enum, & elem(&1, 1) != compare)

  # Apply functions
  def apply_fun(nil), do: & &1
  def apply_fun(fun_), do: fun_

  # Convierte y aplica funcion
  #def to_function(value, nil), do: value
  #def to_function(value, list), do: Enum.reduce(list, value, &apply(string_function(&1["key"]), params(&2, &1["value"])))
  #def params(val1, nil), do: [val1]
  #def params(val1, val2), do: [val1, val2]
  #defp string_function(str), do: Code.eval_string("&#{str}") |> elem(0)
end
