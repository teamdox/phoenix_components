let Wrapper = {
  mounted() {
    var el = this.el
    this.handleEvent("refreshdoc", ({id}) => {
      el.querySelectorAll(id).forEach(node => {var src = node.src;  node.src = src});
    })
  }
};

export {Wrapper};