defmodule PhoenixComponents.Preview.Wrapper do
  use Surface.LiveComponent

  @msview_endpoint "https://view.officeapps.live.com/op/embed.aspx?src="
  @gview_endpoint "https://docs.google.com/viewer?embedded=true&url="

  prop gview, :string, default: @gview_endpoint
  prop node, :any, required: true
  prop opened, :boolean, default: true
  data selected, :string, default: nil
  prop title, :string, default: ""
  prop url_base, :string, required: true
  prop preview, :string, default: "/nodeservice/preview/"
  prop download, :string, default: "/nodeservice/download/"
  prop custom_preview, :string
  prop reload, :boolean, default: false
  prop translate, :keyword
  prop native, :boolean, default: false
  prop delete, :boolean, default: false

  slot header
  slot actions

  @impl true
  def handle_event("delete", %{"unique_id" => unique_id, "parent" => parent}, socket) do
    {:noreply,
      {:removesecundaryparent, unique_id, parent}
      |> PhoenixComponents.call("node")
      |> delete(unique_id, socket)
    }
  end

  def handle_event("refreshdoc", params, socket) do
    {:noreply, socket |> Phoenix.LiveView.push_event("refreshdoc", params)}
  end

  def handle_event("selected", %{"selected" => sel}, %{assigns: %{custom_preview: nil}} = socket) do
    {:noreply, socket |> assign(selected: sel)}
  end

  def handle_event("selected", %{"selected" => sel}, socket) do
    {:noreply, socket |> assign(selected: sel) |> assign(custom_preview: socket.assigns.preview)}
  end

  # Selecciona elemento
  def set_action(id, selected), do: send_update(__MODULE__, id: id, selected: selected)

  # unique_id
  defp unique_id(%{unique_id: id}), do: id
  defp unique_id(%{"unique_id" => id}), do: id

  # Obtiene description
  defp description(%{type: type} = t, [_head|_tail] = d), do: d[String.to_atom(type)] || description(t, nil)
  defp description(%{"type" => type} = t, [_head|_tail] = d), do: d[String.to_atom(type)] || description(t, nil)
  defp description(%{type: type}, _desc), do: String.replace(type, "_", " ")
  defp description(%{"type" => type}, _desc), do: String.replace(type, "_", " ")

  # Obtiene inserted_at
  defp inserted_at(%{inserted_at: inserted_at}), do: inserted_at |> PhoenixComponents.formatdate
  defp inserted_at(%{"inserted_at" => inserted_at}), do: inserted_at |> PhoenixComponents.convert_date |> PhoenixComponents.formatdate

  # Obteniendo mime
  defp src(%{path: path} = params, url), do: params |> min(url, path |> MIME.from_path)
  defp src(%{"path" => path} = params, url), do: params |> min(url, path |> MIME.from_path)

  # Para miniatura
  defp min(child, url, mime) when binary_part(mime, 15, -3) == "vnd", do: "#{@msview_endpoint}#{url}#{child |> unique_id}"
  defp min(child, url, _mime), do: "#{url}#{child |> unique_id}#toolbar=0&navpanes=0&scrollbar=0&view=fitH&statusbar=0&transparent=0"

  # Para fullscreen
  defp full(mime, id, url) when binary_part(mime, 0, 5) == "image", do: "#{url}#{id}#toolbar=0&navpanes=0&scrollbar=0&view=fitH&statusbar=0&transparent=0"
  defp full(_mime, id, url), do: "#{@gview_endpoint}#{url}#{id}"

  defp valid_path(nil, id, url), do: full(nil, id, url)
  defp valid_path(item, id, url), do: Map.get(item, :path) |> MIME.from_path |> full(id, url)

  defp path(id, url, _children, true), do: "#{url}#{id}#toolbar=0&navpanes=0&scrollbar=0&view=fitH&statusbar=0&transparent=0"
  defp path(id, url, children, false), do:
    Enum.find(children, &(unique_id(&1) == id))
    |> valid_path(id, url)

  defp delete({:error, _error}, _unique_id, socket), do: socket
  defp delete({:ok, _item}, unique_id, socket), do: socket |> reload(unique_id)

  defp reload(socket, unique_id) do
    send(self(), {:deleted, unique_id})
    socket
  end
end
