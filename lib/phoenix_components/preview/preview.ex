defmodule PhoenixComponents.Preview do
  use Surface.Component, slot: "preview"
  slot default
end
