defmodule PhoenixComponents.Preview.Wrapper.Header do
  use Surface.Component, slot: "header"
  slot default
end
