defmodule PhoenixComponents.Preview.Wrapper.Actions do
  use Surface.Component, slot: "actions"
  slot default
end
