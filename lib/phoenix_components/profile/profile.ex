defmodule PhoenixComponents.Profile do
  use Surface.LiveComponent

  prop logo, :string, default: "profile"
  prop redirect, :string
  prop ticket, :string
  slot default

  def handle_event("logout", %{"ticket" => ticket, "redirect" => redirect}, socket),
    do: {:noreply, ticket |> logout({socket, redirect})}

  # Eliminar ticket
  def logout(ticket, data), do:
    {:killticket, ticket}
    |> PhoenixComponents.call(:admticket_srv)
    |> process_logout(data)

  def process_logout({:ok, _ignore}, {socket, redirect}), do: push_redirect(socket, to: redirect)
  def process_logout(_, {socket, _}), do: socket
end
