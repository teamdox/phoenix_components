defmodule PhoenixComponents.Table.Simple do
  @moduledoc """
  A simple HTML table.

  You can create a table by setting a souce `data` to it and defining
  columns using the `Table.Column` component.
  """

  use Surface.LiveComponent
  alias PhoenixComponents.Paginate

  @size ~w(xs sm md lg xl)
  @align ~w(text-left text-right text-center justify-start justify-end justify-center)

  @doc "Los datos de la tabla"
  prop records, :list, required: true

  @doc "Habilitar el hover sobre las filas"
  prop hovered, :boolean, default: false

  @doc "Habilitar color zebra"
  prop zebra, :boolean, default: false

  @doc "Habilitar truncate"
  prop truncate, :boolean, default: false

  @doc "The CSS class for the wrapping `<div>` element"
  prop class, :css_class, default: "bg-white shadow-lg overflow-hidden border border-gray-200 rounded-lg"

  @doc "Alineacion del texto en las filas"
  prop align, :string, values: @align, default: "text-left"

  @doc "Tamaño letra head de la tabla"
  prop table_head_size, :string, values: @size, default: "xs"

  @doc "Tamaño letra body de la tabla"
  prop table_body_size, :string, values: @size, default: "sm"

  @doc "Columnas de la tabla"
  slot cols, args: [item: ^records], required: true

  @doc "Paginador en caso que no usar el por defecto"
  slot paginate

  @doc "Pagina actual"
  prop page, :integer

  @doc "Elementos por pagina"
  prop per_page, :integer

  @doc "Total de paginas"
  prop total_page, :integer

  @doc "Total de registros"
  prop total_records, :integer

  @doc "Opciones del select"
  prop options_select, :list

  @doc "Ruta de navegación"
  prop route, :any

  @doc "Parametros de la url"
  prop query_params, :map, default: %{}

  def handle_params(socket, params), do: Paginate.handle_params(socket, params)
end
