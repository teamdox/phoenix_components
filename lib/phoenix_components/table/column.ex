defmodule PhoenixComponents.Table.Column do
  @moduledoc """
  Define una columna para el componente de la tabla principal.

  La instancia de columna se agrega automáticamente a la tabla
  `cols` slot.
  """

  use Surface.Component, slot: "cols"
  @align ~w(text-left text-right text-center justify-start justify-end justify-center)

  @doc "Alineacion del texto en la columna"
  prop align, :string, values: @align

  @doc "Texto del encabezado de columna"
  prop opts, :keyword, default: []
  prop label, :string, required: true
end
