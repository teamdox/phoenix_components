defmodule PhoenixComponents.Filter.Section do
  use Surface.Component, slot: "section"
  require Logger
  alias Surface.Components.Form.{Label, Field, Select, MultipleSelect}
  alias PhoenixComponents.{Dropdown, TextInput, DatePicker, Switch}
  prop class, :css_class, default: "w-full font-base leading-6 uppercase"
  prop opened, :boolean, default: false
  prop row, :boolean, default: false
  prop hidden, :boolean, default: false
  prop gap, :string, default: "4"
  prop label, :string
  slot items

  defp warning_render(type) do
    Logger.warn("No hay un definicion de renderizado para el tipo de campo #{type}.")
    ""
  end
end
