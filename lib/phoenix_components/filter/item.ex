defmodule PhoenixComponents.Filter.Item do
  use Surface.Component, slot: "items"
  @doc "Tipo de campo en el formulario del filtro"
  prop type, :atom, default: :text
  @doc "Label del campo"
  prop label, :string, default: ""
  @doc "Nombre del campo"
  prop name, :string
  @doc "Determina si el campo sera visible o no"
  prop visible, :boolean, default: true
  @doc "Opciones para el campo como clases y otros atributos html"
  prop opts, :keyword, default: []
end
