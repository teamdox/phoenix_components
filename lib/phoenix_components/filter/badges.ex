defmodule PhoenixComponents.Filter.Badges do
  use Surface.LiveComponent

  @doc "Map de traducción parametros"
  prop keys, :map, default: %{}
  @doc "Map parametros de la url"
  prop query_params, :map
  @doc "Identificador del componente filtro"
  prop filter_id, :string
  @doc "Parametro para trabajar sobre la ruta"
  prop route, :any
  prop tz_offset, :number, default: 0

  def handle_event("delete", params, socket), do: socket |> delete(params)

  defp delete(socket, %{"key" => k, "name" => n}) do
    filter = socket.assigns.query_params["filter"]
    socket |> apply_params(%{filter | n => Enum.reject(get_in(filter, [n]), &match?(^k, &1))})
  end
  defp delete(socket, %{"key" => k}), do: apply_params(socket, Map.delete(socket.assigns.query_params["filter"], k))
  defp delete(socket, _), do: socket |> apply_params(%{})

  defp apply_params(%{:assigns => %{:query_params => q} = a} = socket, filter) do
    q = %{q | "filter" => filter}
    send_update(PhoenixComponents.Filter, id: a.filter_id, filter: q["filter"])
    {:noreply, push_patch(socket, to: PhoenixComponents.routes.live_path(socket, a.route, q))}
  end

  defp filter(%{"filter" => filter}), do: filter
  defp filter(_), do: %{}

  # Traductor para el componente Badges en los campos de fechas
  #def badges_traslate_dates(socket, dates_keys) when is_binary(dates_keys), do: badges_traslate_dates(socket, [dates_keys])

  #def badges_traslate_dates(%{assigns: %{query_params: %{"filter" => filters}, keys: keys}} = socket, dates_keys) when is_list(dates_keys) do
  #  date_fields = filters |> Map.take(dates_keys)
  #  values =
  #  for {_k, v} <- date_fields,
  #  into: Map.new(),
  #  do: {v, v |> PhoenixComponents.DatePicker.Helpers.formatt(socket.assigns.time_zone_offset || 0)}
  #
  #  socket |> Phoenix.LiveView.assign(:keys, keys |> Enum.into(values))
  #end

  #def badges_traslate_dates(socket, _dates_keys), do: socket
end
