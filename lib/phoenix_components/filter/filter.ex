defmodule PhoenixComponents.Filter do
  use Surface.LiveComponent

  @doc "En caso de no especificar slot action"
  prop label, :string, default: "Filtrar"

  @doc "Define posición del panel de filtro"
  prop left, :boolean, default: false

  @doc "Define comportamiento"
  prop accordion, :boolean, default: false

  @doc "Define bordes a las secciones"
  prop bordered, :boolean, default: false

  @doc "Size del contenido"
  prop height, :string, default: "auto"

  @doc "Css personalizado para el boton"
  prop action_class, :css_class, default: "bg-blue-600"

  @doc "Header class"
  prop header_class, :css_class, default: "bg-blue-600 py-6 px-4 sm:px-6"

  @doc "Título del Filtro - Elemento de la Cabecera"
  prop title, :string

  @doc "Descripción del Filtro - Elemento de la Cabecera"
  prop description, :string, default: ""

  @doc "Habilitar el hover del boton filtrar"
  prop hover, :event, default: true

  @doc "Objeto que contiene el modelo del formulario"
  data filter, :map, default: %{}

  @doc "Permitira definir una Cabecera personalizada"
  slot header

  @doc "Seccion del filtro"
  slot section

  @doc "En caso de no especificar label para la acción de abrir panel"
  slot action

  @doc "Ruta de navegación"
  prop route, :any

  @doc "Parametros de la url"
  prop query_params, :map, default: %{}

  @doc "API pública. Función que realiza la actualizacion del modelo del formulario a partir de los paremtros en la URL"
  def handle_params(socket, params, filter_id) do
    params
    |> Map.take(["filter"])
    |> update_form(filter_id, socket)
  end

  @doc "API pública. Función que publica a la URL los cambios detectados en el formulario"
  def handle_event("changes", params, %{:assigns => a} = socket) do
    params =
      Enum.into(params, a.query_params)
      |> PhoenixComponents.ignore_keys(["_csrf_token", "target"])

    filter = assign(socket, :filter, params["filter"] || %{})
    {:noreply, push_patch(socket, to: socket.router.__helpers__().live_path(filter, socket.assigns.route, params |> PhoenixComponents.clean_map))}
  end

  defp update_form(%{"filter" => filter}, filter_id, socket) do
    send_update(__MODULE__, id: filter_id, filter: filter)
    socket |> assign(:filter, filter)
  end

  defp update_form(_filter, filter_id, socket) do
    send_update(__MODULE__, id: filter_id, filter: nil)
    socket |> assign(:filter, %{})
  end

  defp transition("enter.start", true), do: "scale-x-0 -translate-x-1/2"
  defp transition("enter.start", false), do: "translate-x-full"
  defp transition("enter.end", true), do: "scale-x-100 translate-x-0"
  defp transition("enter.end", false), do: "translate-x-0"
  defp transition("leave.start", true), do: transition("enter.end", true)
  defp transition("leave.start", false), do: transition("enter.end", false)
  defp transition("leave.end", true), do: transition("enter.start", true)
  defp transition("leave.end", false), do: transition("enter.start", false)
end
