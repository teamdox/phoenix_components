defmodule PhoenixComponents.Accordion.Item do
  use Surface.Component, slot: "items"
  import PhoenixComponents.Accordion, only: [xdata: 1]

  @doc "Item label"
  prop label, :string, default: ""

  slot actions
  slot default
end
