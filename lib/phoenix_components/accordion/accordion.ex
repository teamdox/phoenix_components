defmodule PhoenixComponents.Accordion do
  use Surface.Component
  use Phoenix.HTML

  prop collapse, :boolean, default: false
  prop rounded, :boolean, default: false
  prop px, :string, default: "3"
  prop py, :string, default: "2"
  prop iconColor, :string, default: "gray-600"
  prop class, :string, default: "text-gray-600 font-medium"

  slot items
  slot default

  def xdata(false), do: %{"x-data" => "{selected: null}"}
  def xdata(_ignore), do: %{}
end
