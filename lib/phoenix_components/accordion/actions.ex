defmodule PhoenixComponents.Accordion.Item.Actions do
  use Surface.Component, slot: "actions"

  slot default
end
