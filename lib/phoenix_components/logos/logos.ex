defmodule PhoenixComponents.Logos do
  use Surface.Component

  prop index, :string, default: "content"
  prop class, :css_class, default: "inline-flex items-center space-x-4 py-2"
  slot item

  def get(%{item: item, index: index}) do
    filter = Enum.filter(item, & &1[:type] == "uniqueid")
    keys = Enum.map(filter, & &1[:tenant])

    elastic(filter)
    |> search(index)
    |> get_in(["hits", "hits"])
    |> format()
    |> Kernel.++(item)
    |> Enum.group_by(& &1[:tenant])
    |> Enum.sort_by(&Enum.find_index(keys, fn x -> x == elem(&1, 0) end))
    |> Enum.map(&concat/1)
  end

  defp elastic([]), do: nil
  defp elastic(filter), do:
   %{"bool" =>
      %{"should" => Enum.map(filter, & %{"bool" => %{"must" => [%{"term" => %{"tenant" => &1[:tenant]}}, %{"wildcard" => %{"name" => "#{&1[:name]}.*"}}]}})
      }
    }

  defp concat({_key, [head | tail]}), do: Enum.concat([head | Enum.take(tail, -1)]) |> Enum.into(%{})
  defp concat(_ignore), do: %{}

  defp format([_head | _tail] = enum), do: Enum.map(enum, &format/1)
  defp format(%{"_source" => %{"tenant" => tenant} = val}), do: %{unique_id: val["unique_id"], tenant: tenant}
  defp format(_ignore), do: []

  defp search(nil, _index), do: %{}
  defp search(filter, index) do
    try do
      case :pg2.get_closest_pid(:searchservice) do
        {:error, _ignore} -> %{}
        pid -> GenServer.call(pid, {:complex_system_match, index, "estatico", filter, 0, 10000, "inserted_at:desc" })
      end
    catch
      _what, _value -> %{}
    end
  end
end
