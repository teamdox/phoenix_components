defmodule PhoenixComponents.Logos.Item do
  use Surface.Component, slot: "item"

  prop route, :any
  prop name, :string, default: "logo"
  prop tenant, :string, default: ""
  prop class, :css_class, default: []
  prop type, :string, default: "uniqueid"
end
