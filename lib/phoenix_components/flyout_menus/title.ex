defmodule PhoenixComponents.FlyoutMenus.Title do
  use Surface.Component, slot: "title"

  slot default
end
