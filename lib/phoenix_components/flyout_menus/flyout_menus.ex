defmodule PhoenixComponents.FlyoutMenus do
  use Surface.Component

  @doc "class active"
  prop activeClass, :css_class, default: ~w(border-b-4 text-gray-600 border-primary-200)
  @doc "class hover"
  prop textMenuHover, :string, default: "gray-600"
  @doc "text color"
  prop textMenu, :string, default: "gray-400"
  @doc "bg menu"
  prop bgMenu, :string, default: "white"
  @doc "link selected"
  prop selected, :string
  @doc "bg hover links"
  prop bgLinkHover, :string, default: "gray-100"
  @doc "text color"
  prop textLink, :string
  @doc "text hover links"
  prop textLinkHover, :string
  @doc "text selected link"
  prop textLinkActive, :string, default: "primary-200"
  @doc "margin top menu"
  prop marginTop, :string, default: "2"
  @doc "padding x"
  prop linkWidth, :string, default: "4"
  @doc "padding y"
  prop linkHeight, :string, default: "2"
  @doc "runded size"
  prop rounded, :string, default: "md"
  @doc "shadow"
  prop shadow, :string, default: "lg"
  @doc "title"
  prop label, :string, default: "Label"

  slot title
  slot links
  slot default

  defp isactive(links, selected), do: Enum.map(links, & &1[:tag]) |> Enum.any?(& selected =~ &1)
end
