defmodule PhoenixComponents.FlyoutMenus.Link do
  use Surface.Component, slot: "links"

  @doc "tag"
  prop tag, :string
  @doc "Link"
  prop route, :any
  @doc "whitespace"
  prop whitespace, :string, default: "nowrap"
  @doc "show condition"
  prop hidden, :boolean, default: false

  slot default

  def render(assigns), do: ~F"<#slot />"
end
