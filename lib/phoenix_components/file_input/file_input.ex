defmodule PhoenixComponents.FileInput do
  use Surface.LiveComponent

  prop accept, :string
  prop name, :string, required: true
  prop multiple, :boolean, default: false
  prop webkitdirectory, :boolean, default: false
  prop mozdirectory, :boolean, default: false
  prop class_select, :css_class, default: "flex max-w-xs"
  prop class_group, :css_class, default: "mt-1 max-w-2xl bg-white overflow-hidden rounded-md border text-sm"
  prop label, :string, default: "Adjuntar Documento"
  data files, :list, default: []
  prop options, :keyword, default: []

  def handle_event("uploadfiles", files, socket) do
    {:noreply, socket |> update(:files, &apply_files(&1, files, socket))}
  end

  def handle_event("remove", %{"uuid" => uuid}, %{assigns: %{files: files}} = socket) do
    {:noreply, socket |> assign(files: Enum.filter(files, & &1["uuid"] != uuid))}
  end

  def upload_file(id, files), do: send_update(__MODULE__, id: id, files: files)

  defp apply_files(f, files, %{assigns: %{multiple: true}}), do: Enum.uniq(f ++ files)
  defp apply_files(_, files, _), do: files

  defp get_size(size) when size > 1048576 , do: "#{round(size / 1048576)} mb"
  defp get_size(size) when size > 1024 and size < 1048576, do: "#{round(size / 1024)} kb"
  defp get_size(size), do: "#{size} b"
end
