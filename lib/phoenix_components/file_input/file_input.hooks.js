let FileInput = {
  mounted() {
    window.uploadFiles = (ref, type, desc) => processfiles("uploadfiles", ref, type, desc, this)
  }
};

const processfiles = (event, ref, type, desc, el) => {
  let readers = [], 
      id = "#" + ref.id,
      files = ref.files
      
  // Abortar si no hubo archivos seleccionados
  if(!files.length) return el.pushEventTo(id, event, [])
  
  // Almacenar promesas en matriz
  Array.from(files).forEach(file => readers.push(toBase64(file, type, desc)))
  // Promesas desencadenantes
  Promise.all(readers).then((values) => {el.pushEventTo(id, event, values)});
}

const toBase64 = (file, type, desc) => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => resolve(
      { binary: reader.result.split(',', 2)[1]
      , uuid: crypto.randomUUID()
      , lastModified: file.lastModified
      , lastModifiedDate: file.lastModifiedDate
      , name: file.name
      , size: file.size
      , mimetype: file.type
      , type: type
      , description: desc
      , webkitRelativePath: file.webkitRelativePath
      }
  );
  reader.onerror = error => reject(error);
});

export {FileInput};