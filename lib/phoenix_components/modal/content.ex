defmodule PhoenixComponents.Modal.Content do
  use Surface.Component, slot: "content"

  prop class, :css_class, default: []
  slot default

  def render(assigns) do
    ~F"""
    <div {=@class}>
        <#slot />
    </div>
    """
  end
end
