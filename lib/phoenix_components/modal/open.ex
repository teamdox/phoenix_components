defmodule PhoenixComponents.Modal.Open do
  use Surface.Component, slot: "open"

  @doc "prop class"
  prop class, :css_class, default: "py-2 px-4 text-sm leading-5 text-white focus:ring-0 font-medium rounded-md transition duration-150 ease-in-out bg-primary-300"
  @doc "prop label"
  prop label, :string, default: "Abrir"
  @doc "prop data"
  prop icon, :boolean, default: false
  prop type, :string, values: ["external", "local", "url"], default: "external"

  slot default

  def render(assigns) do
    ~F"""
    <Context get={ open: open, id: id}>
      <div class="flex justify-end">
        <#slot>
          {#if @icon}
            <span class="cursor-pointer text-gray-600" :on-click={open} phx-value-id={id}> <PhoenixComponents.Svg {=@type} name={@label} /> </span>
          {#else}
            <button :on-click={open} phx-value-id={id} type="button" {=@class}>
              {@label}
            </button>
          {/if}
        </#slot>
      </div>
    </Context>
    """
  end
end
