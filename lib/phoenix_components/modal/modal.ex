defmodule PhoenixComponents.Modal do
  use Surface.LiveComponent

  @doc "Tamaño modal"
  prop width, :string, default: "xl"

  @doc "Mostrar/Ocultar modal"
  data show, :boolean, default: false

  @doc "Evento para abrir modal"
  prop open_modal, :event, default: "open_modal"

  slot open
  slot header
  slot footer
  slot content

  def set_action(id, show), do: send_update(__MODULE__, id: id, show: show)

  def handle_event("close_modal", _params, socket) do
    {:noreply, socket |> assign(:show, false)}
  end
end
