defmodule PhoenixComponents.Modal.Footer do
  use Surface.Component, slot: "footer"

  prop class, :css_class, default: "rounded-b-lg px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse"
  slot default

  def render(assigns) do
    ~F"""
    <div {=@class}>
      <#slot />
    </div>
    """
  end
end
