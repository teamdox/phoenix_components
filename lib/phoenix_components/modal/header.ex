defmodule PhoenixComponents.Modal.Header do
  use Surface.Component, slot: "header"

  slot default

  def render(assigns) do
    ~F"""
    <h3 class="text-lg leading-6 font-medium text-gray-900">
      <#slot />
    </h3>
    """
  end
end
