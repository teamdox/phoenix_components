defmodule PhoenixComponents.RoutePatch do
  use Surface.Component
  alias Surface.Components.{LivePatch, LiveRedirect}

  @doc "Cuando no se proporciona contenido (default slot)"
  prop label, :string
  @doc "class css"
  prop class, :css_class, default: []
  @doc "Opciones adicionales"
  prop opts, :keyword, default: []
  @doc "Ruta url"
  prop route, :any, required: true
  prop redirect, :boolean, default: false

  @doc """
    Si no se especifica contenido, en su lugar
    se utiliza el valor de la propiedad `label`
  """
  slot default

  def render(%{:redirect => false} = assigns) do
    ~F"""
    <LivePatch to={to(@socket, @route, @opts)} {=@class}>
        <#slot>{@label}</#slot>
    </LivePatch>
    """
  end

  def render(assigns) do
    ~F"""
    <LiveRedirect to={to(@socket, @route, @opts)} {=@class}>
        <#slot>{@label}</#slot>
    </LiveRedirect>
    """
  end

  @doc """
    Alternativa para las vistas
    1. import PhoenixComponents.RoutePatch
    2. <.route_patch label="Volver" route={MyApp.Nav.Home} conn={@conn} />
  """
  def route_patch(assigns) do
    ~H"""
    <%= Phoenix.LiveView.Helpers.live_patch @label, to: PhoenixComponents.routes.live_path(@conn, @route, get_in(assigns, [Access.key(:opts, [])])) %>
    """
  end

  defp to(_socket, route, _opts) when is_binary(route), do: route
  defp to(socket, route, opts), do: socket.router.__helpers__().live_path(socket, route, opts)
end
