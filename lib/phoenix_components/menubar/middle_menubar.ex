defmodule PhoenixComponents.MiddleMenubar do
  @moduledoc """
    Documentación para MiddleSidebar
  """
  use Surface.Component

  @doc "class css"
  prop class, :css_class, default: []

  slot left
  slot right
end
