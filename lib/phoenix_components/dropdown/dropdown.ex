defmodule PhoenixComponents.Dropdown do
  use Surface.LiveComponent
  alias PhoenixComponents.Svg

  prop form, :any, required: true
  prop name, :string, required: true
  data opened, :boolean, default: false
  prop selected, :any
  prop options, :keyword, default: []
  prop opts, :keyword, default: []
  prop placeholder, :string, default: "Seleccione una opción"

  def handle_event("dropdown_toggle", _params, socket) do
    {:noreply, update(socket, :opened, &(!&1))}
  end
  defp selected_options(options, params), do: (Enum.map(options, fn {_, val} -> val end)) -- params == []
  defp translate(map, key), do: map |> PhoenixComponents.convert_to_map |> Map.get(key, key)
end
