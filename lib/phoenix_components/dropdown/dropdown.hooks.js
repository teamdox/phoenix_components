let Dropdown = {
  mounted(){
    window.checkedAll = (flag, name) => toggleChecked(flag, document.getElementsByName(name))
    window.removeChecked = (key) => toggleChecked(false, [document.getElementById(key)])
  }
};

const toggleChecked = (flag, checkboxes) => {
  [...checkboxes].map(el => { el.checked = flag })
}
export {Dropdown};