defmodule PhoenixComponents.Tabs.TabItem do
  use Surface.Component, slot: "tabs"

  @doc "Item label"
  prop label, :string, default: ""
  @doc "Hidden"
  prop hidden, :boolean, default: false
  @doc "Class"
  prop class, :css_class, default: []
  @doc "Count"
  prop count, :string
end
