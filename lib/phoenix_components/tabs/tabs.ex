defmodule PhoenixComponents.Tabs do
  use Surface.Component

  @doc "Line Color"
  prop line, :string, default: "gray-300"
  @doc "Tab vertical"
  prop vertical, :boolean, default: false
  @doc "Primary Color"
  prop primary, :string, default: "indigo-500"
  @doc "Estilo para los enlaces"
  prop class, :css_class, default: "border-transparent whitespace-nowrap flex py-2 px-1 border-b-2 font-medium text-sm"
  slot tabs

  defp actived(enumerable), do: Enum.find_index(enumerable, &not(&1[:hidden]))
end
