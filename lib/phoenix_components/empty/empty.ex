defmodule PhoenixComponents.Empty do
  @moduledoc """
  Documentation for Empty.
  """
  use Surface.Component

  prop description, :string, default: "No se han encontrado resultados"
  prop class, :css_class, default: "w-full bg-white shadow-lg overflow-hidden border border-gray-200 py-20 text-center sm:rounded-lg"
end
