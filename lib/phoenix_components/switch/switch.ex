defmodule PhoenixComponents.Switch do
  use Surface.LiveComponent

  prop value, :string, default: nil
  prop name, :string
  prop class, :css_class
  prop primary, :string, default: "primary-200"
  slot default
end
