defmodule PhoenixComponents.Timeline.Date do
  use Surface.Component

  prop value, :string
  prop tz_offset, :integer, default: 0
  slot default

  def render(assigns) do
    ~F"""
      { Timex.parse(@value, "{ISO:Extended:Z}") |> convert(@tz_offset) }
    """
  end

  defp convert({:ok, date}, tz_offset), do: Timex.Timezone.convert(date, Timex.Timezone.name_of(tz_offset)) |> format()
  defp convert({:error, err}, _tz_offset), do: err

  defp format(%{day: d, month: m, year: y, hour: h, minute: min}), do: "#{d} de #{translate(m)} del #{y} #{h}:#{min}"

  defp month(number), do: Timex.month_name(number)
  defp translate(number), do: Timex.Translator.translate("es", "months", month(number))
end
