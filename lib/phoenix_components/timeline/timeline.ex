defmodule PhoenixComponents.Timeline do
  use Surface.Component
  use Timex

  prop key, :string
  prop line_gradient, :string, default: "#0070c0"
  slot status
  slot rows
end
