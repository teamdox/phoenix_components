defmodule PhoenixComponents.Timeline.Rows do
  use Surface.Component, slot: "rows"

  prop records, :list, required: true
  slot item, args: [item: ^records], required: true
  slot title, args: [item: ^records]
  slot default

  def get(nil, _item), do: true
  def get(key, item), do: item[key]

  def statinfo(nil, _key, _token), do: []
  def statinfo(status, key, token), do: Enum.find(status, & &1[:name] == key)[token]

  defp gradient(index, total, <<"#", _rest::binary>> = color), do: style(index, total, color)
  defp gradient(index, total, color), do: class(index, total, color)

  defp style(0, 0, color), do: [class: "bg-gradient-to-t from-white to-white", style: via_color(color)]
  defp style(0, _, color), do: [class: "bg-gradient-to-t", style: "#{from_color(color)}#{via_color(color)}#{bg_image("to top")}"]
  defp style(index, total, color) when index == total, do: [class: "bg-gradient-to-b", style: "#{from_color(color)}#{via_color(color)}#{bg_image("to bottom")}"]
  defp style(_index, _total, color), do: [style: "background: #{color}"]

  defp class(0, 0, color), do: [class: "bg-gradient-to-t from-white to-white via-#{color}"]
  defp class(0, _, color), do: [class: "bg-gradient-to-t from-#{color}"]
  defp class(index, total, color) when index == total, do: [class: "bg-gradient-to-b from-#{color}"]
  defp class(_index, _total, color), do: [class: "bg-#{color}"]

  defp via_color(color), do: "--tw-gradient-stops: var(--tw-gradient-from),#{color},var(--tw-gradient-to, #{color}00);"
  defp from_color(color), do: "--tw-gradient-from: #{color};"
  defp bg_image(dir), do: "background-image: linear-gradient(#{dir},var(--tw-gradient-stops));"

  defp bg_color(<<"#", _rest::binary>> = color), do: [style: "background: #{color}"]
  defp bg_color(color), do: [class: "bg-#{color}"]

  defp keyword_merge(keyword) do
    Enum.group_by(keyword, &elem(&1, 0), &elem(&1, 1) <> " ")
    |> Enum.to_list
  end
end
