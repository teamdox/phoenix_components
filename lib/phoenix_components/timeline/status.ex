defmodule PhoenixComponents.Timeline.Status do
  use Surface.Component, slot: "status"

  prop name, :string
  prop color, :string
  prop description, :string
  prop svg, :string
end
