defmodule PhoenixComponents.Timeline.Rows.Title do
  use Surface.Component, slot: "title"

  prop class, :css_class, default: "text-sm leading-6 font-medium text-gray-900 px-1"
  slot default
end
