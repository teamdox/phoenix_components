defmodule PhoenixComponents.Timeline.Rows.Item do
  use Surface.Component, slot: "item"

  prop key, :string
  prop label, :string
  slot default
end
