defmodule PhoenixComponents.Navmenu do
  use Surface.Component

  @doc "class active"
  prop active, :string
  @doc "class css"
  prop class, :css_class, default: []
  prop hidden, :boolean, default: false

  slot title
  slot default
end
