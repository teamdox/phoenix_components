defmodule PhoenixComponents.Navmenu.Title do
  use Surface.Component, slot: "title"

  slot default
end
