defmodule PhoenixComponents.Paginate do
  use Surface.LiveComponent
  alias Surface.Components.{Form, LivePatch}

  @doc "Pagina actual"
  prop page, :integer, default: 1

  @doc "Elementos por pagina"
  prop per_page, :integer, default: 10

  @doc "Total de paginas"
  prop total_page, :integer, default: 0

  @doc "Total de registros"
  prop total_records, :integer, default: 0

  @doc "Opciones del select"
  prop options_select, :list, default: [5, 10, 15, 20, 100]

  @doc "Ruta de navegación"
  prop route, :any

  @doc "Parametros de la url"
  prop query_params, :map, default: %{}

  @doc "Actualiza el modelo con los paremtros en la URL"
  def handle_params(socket, params) do
    socket
    |> assign(page: (params["page"] || 1) |> to_integer)
    |> assign(per_page: (params["per_page"] || 10) |> to_integer)
    |> update_total
    |> process_params
  end

  @doc "Actualiza URL con los parametros del evento"
  def handle_event("changes", params, %{:assigns => a} = socket) do
    params =
      Enum.into(params, a.query_params)
      |> PhoenixComponents.ignore_keys(["_csrf_token", "target"])
      |> conv_valid_params(socket)

    paginate = assign(socket, params)
    {:noreply, push_patch(socket, to: socket.router.__helpers__().live_path(paginate, socket.assigns.route, params |> PhoenixComponents.clean_map))}
  end

  def handle_event("submit", params, %{:assigns => a} = socket) do
    params =
      Enum.into(params, a.query_params)
      |> PhoenixComponents.ignore_keys(["_csrf_token", "target"])
      |> conv_valid_params(socket)

    paginate = assign(socket, params)
    {:noreply, push_patch(socket, to: socket.router.__helpers__().live_path(paginate, socket.assigns.route, params |> PhoenixComponents.clean_map))}
  end

  # Validar el numero de la pagina actual
  defp process_params(%{page: page} = socket) when is_integer(page) and page < 1, do: socket |> assign(page, 1)
  defp process_params(%{page: page} = socket) when is_integer(page), do: socket
  defp process_params(%{page: page} = socket) when is_binary(page),
    do: socket |> assign(page, page |> to_integer) |> process_params
  defp process_params(socket), do: socket

  # Convierte params a %{key: valor}
  defp conv_valid_params(params, socket) do
    %{params |
      "page" => params["page"] |> to_integer,
      "per_page" => params["per_page"] |> to_integer
    } |> Enum.map(fn ({key, val}) -> {String.to_atom(key), val} end)
      |> Enum.into(%{})
      |> valida_page(socket)
  end

  # Actualiza la propiedad total_page con el total de paginas
  defp update_total(%{:assigns => %{total_records: r, per_page: p}} = socket),
    do: socket |> assign(total_page: r |> total(p))

  # Valida el defase de paginas al cambiar cantidad elementos por pagina
  defp valida_page(%{page: page, per_page: per_page} = params, %{:assigns => %{total_records: t}})
    when ceil((page * per_page) / per_page) > ceil(t/per_page),
    do: %{params | page: total(t, per_page)}

  defp valida_page(params, _socket), do: params

  # Calcula total paginas
  def total(total, per_page), do: ceil(total/to_integer(per_page))

  # Convierte a entero el parametro
  defp to_integer(num) when is_integer(num), do: num
  defp to_integer(num) when is_binary(num), do: String.to_integer(num)
  defp to_integer(_), do: 1

  defp to_path(%{:route => route, :per_page => per_page, :query_params => qs, :socket => socket}, page),
    do: socket.router.__helpers__().live_path(socket, route, Enum.into(%{page: page, per_page: per_page}, qs))

  # Deshabilitando el paginador
  defp isdisabled(true), do: "text-gray-300 bg-gray-50 border-gray-300 pointer-events-none"
  defp isdisabled(_), do: ""
end
