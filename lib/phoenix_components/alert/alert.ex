defmodule PhoenixComponents.Alert do
  use Surface.LiveComponent
  alias PhoenixComponents.Svg

  @doc "Tamaño modal"
  prop width, :string, default: "lg"

  @doc "Título del Alert"
  prop title, :string, required: true

  @doc "Descripcion de la alerta"
  prop description, :string

  @doc "Tipo de Alerta"
  prop type, :atom, default: :info, values!: [:info, :warn, :error, :succes, :cancelled, :question]

  @doc "Button submit Label"
  prop ok_label, :string, default: "Aceptar"

  @doc "Button submit Label"
  prop close_label, :string, default: "Cancelar"

  @doc "datos que se desean enviar al cerrar o hacer ok"
  prop data, :keyword, default: []

  @doc "Uppercase option"
  prop uppercase, :boolean, default: false

  @doc "Evento ok action"
  prop ok_event, :event, default: "close"

  @doc "Evento de abrir el modal"
  prop open_event, :event, default: "open"

  @doc "Mostrar/Ocultar modal"
  data show, :boolean, default: false

  @doc "Elemento html para abrir el modal"
  slot open, required: true
  @doc "Cuerpo del modal"
  slot default
  @doc "Definicion de botones"
  slot actions

  def handle_event("open", _, socket) do
    {:noreply,  socket |> assign(show: true)}
  end

  def handle_event("close", _params, socket) do
    {:noreply,  socket |> assign(show: false)}
  end

  # Public API
  def open(modal_id) do
    send_update(__MODULE__, id: modal_id, show: true)
  end

  def close(modal_id) do
    send_update(__MODULE__, id: modal_id, show: false)
  end

end
