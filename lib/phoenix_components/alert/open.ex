defmodule PhoenixComponents.Alert.Open do
  use Surface.Component, slot: "open"

  @doc "prop class"
  prop class, :css_class, default: "py-2 px-4 text-white hover:bg-opacity-90"
  prop label, :string

  slot default

  def render(assigns) do
    ~F"""
      <Context get={PhoenixComponents.Alert, uppercase: uppercase}>
        <div class={"cursor-pointer text-sm leading-5 focus:outline-none focus:ring-0 font-medium rounded-md bg-primary-components transition duration-150 ease-in-out", @class, " uppercase": uppercase}>
          <#slot > {@label} </#slot>
        </div>
      </Context>
    """
  end
end
