defmodule PhoenixComponents.Tooltip do
  @moduledoc """
  Documentation for Tooltip.
  """
  use Surface.Component

  prop tip, :string, required: true
  prop class, :css_class, default: ""
  prop tip_class, :css_class, default: "translate-y-1 translate-x-2"
  slot default, required: true
end
