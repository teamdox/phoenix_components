defmodule PhoenixComponents.Svg do
  use Surface.Component

  prop name, :string, required: true
  prop type, :string, values: ["external", "local", "url", "uniqueid"], default: "external"
  prop class, :css_class, default: []

  def render(assigns), do: ~F"{svg_path(assigns)}"

  @doc """
    Alternativa para las vistas
    1. import PhoenixComponents.Svg
    2. <.svg name="404" />
  """
  def svg(assigns) when assigns.type == nil, do: ~H"<%= svg_path(assigns) %>"
  def svg(assigns) do
    assigns = assigns |> Enum.into(%{:type => "external"})
    ~H"<%= svg_path(assigns) %>"
  end

  @doc """
    Cargar imagen
  """
  def svg_path(%{:type => "url"} = assigns), do: {:safe,"<img src=#{assigns[:name]} class=#{assigns[:class] |> Enum.join(" ")} />"}
  def svg_path(%{:type => "uniqueid"} = assigns) do
    {:safe,
      cond do
        not String.valid?(content(assigns[:name])) ->
          "<img class=#{assigns[:class] |> Enum.join(" ")} src=\"#{content(assigns[:name]) |> extmake()}\" />"
        true ->
          content(assigns[:name])
          |> Floki.attr("svg", "class", fn _ -> assigns[:class] |> Enum.join(" ") end)
          |> Floki.raw_html()
      end
    }
  end

  def svg_path(%{:name => name } = assigns) do
    {:safe,
      appdir(assigns, "priv/static/svg/#{name}.svg")
      |> File.read()
      |> loadsvg
      |> Floki.parse_fragment()
      |> insert_class(get_in(assigns, [Access.key(:class, [])]))
    }
  end

  defp appdir(%{:type => "external"}, name), do: Application.app_dir(:svglobal, name)
  defp appdir(%{:type => "local"}, name), do: System.fetch_env!("APP_NAME") |> String.to_atom |> Application.app_dir(name)

  defp insert_class({:error, html}, _), do: raise("No se puede parsear el html\n#{html}")
  defp insert_class({:ok, html}, class) when is_nil(class) or class == [], do: Floki.raw_html(html)
  defp insert_class({:ok, html}, class) do
    Floki.attr(html, "svg", "class", fn _ -> Enum.join(class, " ") end)
    #Floki.attr(html, "svg", "class", &String.trim("#{&1} #{class}"))
    |> Floki.raw_html()
  end

  defp loadsvg({:ok, result}), do: String.trim(result)
  defp loadsvg(_), do: "<svg class='h-5 w-20'><text x='0' y='15' fill='red'>Not Found</text></svg>"

  # Capturando extension
  defp extmake(<<0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, _::binary>> = val), do: "data:image/png;base64,#{Base.encode64(val)}"
  defp extmake(<<0xff, 0xD8, _::binary>> = val), do: "data:image/jpeg;base64,#{Base.encode64(val)}"
  defp extmake(_val), do: ""

  # Leer contenido de alberto
  def content(unique_id) do
    try do
      case :pg2.get_closest_pid("node") do
        {:error, _ignore} -> ""
        pid -> GenServer.call(pid, {:nodecontent_m, unique_id}) |> from_monad()
      end
    catch
      _what, _value -> ""
    end
  end

  defp from_monad({:ok, info}), do: info
  defp from_monad(_ignore), do: ""
end
