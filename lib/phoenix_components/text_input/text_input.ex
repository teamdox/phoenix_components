defmodule PhoenixComponents.TextInput do
  @moduledoc """
  Documentation for TextInput.
  El componente agrega a la defincion del input phx-debounce="1000" como mejora a la hora de filtrar
  """
  use Surface.Component
  alias Surface.Components.Form.Input.InputContext

  # prop form, :any, required: true
  prop name, :string
  prop value, :string
  prop placeholder, :string
  prop opts, :keyword, default: []
  prop debounce, :string, default: "0"
  prop class, :css_class, default: "block w-full border border-gray-300 shadow-sm focus:ring-gray-300 focus:border-gray-300 sm:text-sm rounded-md placeholder-gray-400"


  defp attrs(form, field, name) when is_nil(form) or is_nil(field), do: [name: name]

  defp attrs(form, field, _) do
    [for: Phoenix.HTML.Form.input_id(form, field), name: Phoenix.HTML.Form.input_name(form, field)]
  end
end
