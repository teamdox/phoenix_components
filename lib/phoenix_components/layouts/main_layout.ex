defmodule PhoenixComponents.MainLayout do
  use Surface.Component
  @doc "Clases del main"
  prop class, :css_class, default: []
  @doc "Agrega/Elimina clase relative al main"
  prop relative, :boolean, default: false

  slot header
  slot navbar
  slot footer
  slot default
end
