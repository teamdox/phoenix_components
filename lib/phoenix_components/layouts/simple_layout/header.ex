defmodule PhoenixComponents.SimpleLayout.Header do
  use Surface.Component, slot: "header"

  @doc "class css"
  prop class, :css_class, default: []

  slot left
  slot right
  slot profile
  slot navbar
end
