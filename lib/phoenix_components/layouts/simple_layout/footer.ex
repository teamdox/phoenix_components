defmodule PhoenixComponents.SimpleLayout.Footer do
  use Surface.Component, slot: "footer"
  prop class, :css_class, default: []

  slot default

  def render(assigns) do
    ~F"""
      <footer {=@class}> <#slot /> </footer>
    """
  end
end
