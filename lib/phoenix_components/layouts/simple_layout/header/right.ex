defmodule PhoenixComponents.SimpleLayout.Header.Right do
  use Surface.Component, slot: "right"
end
