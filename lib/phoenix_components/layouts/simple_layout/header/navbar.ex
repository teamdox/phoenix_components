defmodule PhoenixComponents.SimpleLayout.Header.Navbar do
  use Surface.Component, slot: "navbar"
  prop class, :css_class, default: []

  slot default

  def render(assigns) do
    ~F"""
      <nav class={"z-10", @class}> <#slot /> </nav>
    """
  end
end
