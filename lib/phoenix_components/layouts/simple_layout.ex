defmodule PhoenixComponents.SimpleLayout do
  use Surface.Component

  slot header
  slot footer
  slot default
end
