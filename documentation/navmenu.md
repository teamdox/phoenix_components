## PhoenixComponents.Navmenu

* Integración en la vista.
* Ejemplo menu de perfil
```html
    <PhoenixComponents.Navmenu class="cursor-pointer flex items-center max-w-xs text-sm rounded-full text-primary-200 focus:outline-none focus:shadow-outline">
        <:title> <PhoenixComponents.Svg name="profile" /> </:title>
        <div class="max-w-lg max-w-xs px-3 absolute top-6 w-48" style="left: -9rem;">
            <div class="border-0 border-gray-200 border-solid border-l border-t w-2 h-2 bg-white relative transform rotate-45" style="top: 4px; left: 83%;"></div>
            <div class="border bg-white rounded shadow">
                <div class="rounded shadow overflow-hidden">
                    <div class="z-20 p-2">
                        <Surface.Components.Link label="Mi Perfil" to="/miperfil" class="hover:bg-gray-100  block px-4 py-2 text-sm text-gray-700 transition ease-in-out duration-150" />
                        <a href="#" :on-click="logout" phx-value-redirect={@redirect} phx-value-ticket={@ticket} class="cursor-pointer hover:bg-gray-100  block px-4 py-2 text-sm text-gray-700 transition ease-in-out duration-150">Cerrar Sesión</a>
                    </div>
                </div>
            </div>
        </div>
    </PhoenixComponents.Navmenu>
```

* Ejemplo menu sábana
```html
    <PhoenixComponents.Navmenu active="text-gray-600 border-primary-300" class="cursor-pointer inline-flex py-2 px-2 mr-4 text-gray-400 hover:text-gray-600 border-b-4 border-white hover:border-primary-300 transition duration-300">
        <:title> Solicitar Servicios </:title>
        <div class="mt-0.5 absolute z-10 inset-x-0 transform shadow-lg">
            <div class="absolute inset-0 flex" aria-hidden="true">
                <div class="bg-white w-1/2"></div>
                <div class="bg-gray-50 w-1/2"></div>
            </div>
            <div class="relative mx-auto grid grid-cols-1 lg:grid-cols-2">
                <nav class="space-y-1 grid grid-cols-1 lg:grid-cols-3" x-data="{ tab: 'tab1' }">
                <ul>
                    <li class="mt-2 mb-4">
                    <span @mouseover="tab = 'tab1'" :class="{'text-primary-200 border-primary-300':tab === 'tab1'}" class="cursor-pointer block px-5 border-l-4 border-white transition duration-300">
                        Transferencia
                        <span x-show="tab === 'tab1'"><PhoenixComponents.Svg name="arrowright" /></span>
                    </span>
                    </li>
                    <li>
                    <span @mouseover="tab = 'tab2'" :class="{'text-primary-200 border-primary-300':tab === 'tab2'}" class="cursor-pointer block px-5 border-l-4 border-white transition duration-300">
                        Dación y Adjudicación
                        <span x-show="tab === 'tab2'"><PhoenixComponents.Svg name="arrowright" /></span>
                    </span>
                    </li>
                </ul>
                    <div class="pr-10" x-show="tab === 'tab1'">
                        <div class="py-1 text-primary-200 text-sm block pr-8 border-b-2 border-gray-400">Transferencia con Factura</div>
                        <ControlfinancieroWeb.Components.LivePatch hover={false} label="Transferencia con Factura y Pago de Impuesto" {=@routes} route={ControlfinancieroWeb.Nav.Home} />
                    </div>
                    <div class="pr-10" x-show="tab === 'tab2'">
                        <div class="py-1 text-primary-200 text-sm block pr-8 border-b-2 border-gray-400">Dación</div>
                        <ControlfinancieroWeb.Components.LivePatch hover={false} label="Dación de Vehículos" {=@routes} route={ControlfinancieroWeb.Nav.Home} />
                    </div>
                    <div class="pr-10" x-show="tab === 'tab2'">
                        <div class="py-1 text-primary-200 text-sm block pr-8 border-b-2 border-gray-400">Adjudicación</div>
                        <ControlfinancieroWeb.Components.LivePatch hover={false} label="Adjudicación de Vehículos" {=@routes} route={ControlfinancieroWeb.Nav.Home} />
                    </div>
                </nav>
                <div class="bg-gray-50 px-4 py-4">
                <div>
                    <h3 class="flex text-sm font-medium tracking-wide text-gray-500 uppercase">
                        <PhoenixComponents.Svg name="portfolio" />
                        <span class="ml-2">Portafolio de Servicios</span>
                    </h3>
                    <ul class="mt-6 space-y-6">
                        <li class="flow-root">
                            <a href="#" class="-m-3 p-3 flex rounded-lg hover:bg-gray-100 transition ease-in-out duration-150">
                            <div class="sm:block flex-shrink-0 mr-3">
                                <PhoenixComponents.Svg name="services" />
                            </div>
                            <div class="min-w-0 flex-1">
                                <h5 class="text-base font-medium text-gray-900 truncate">
                                    Servicios
                                </h5>
                                <p class="mt-1 text-sm text-gray-500">
                                    Muestra un resumen de los servicios contratados, los no contratados e información acerca de los nuevos servicios desarrollados.
                                </p>
                            </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="mt-6 text-sm font-medium">
                    <a href="#" class="flex items-center text-primary-200 transition ease-in-out duration-150">
                    Ver Todos los Servicios
                    <span class="ml-4"><PhoenixComponents.Svg name="servicesarrow" /></span>
                    </a>
                </div>
                </div>
            </div>
        </div>
    </PhoenixComponents.Navmenu>
```