## PhoenixComponents.Timeline
### Definiendo alias y payload de ejemplo
```elixir
  alias PhoenixComponents.{
    Timeline,
    Timeline.Date,
    Timeline.Rows,
    Timeline.Rows.Item,
    Timeline.Rows.Title,
    Timeline.Status
  }

data records, :list, default: [
  %{"name" => "Actualizar Cuenta", "ini" => "2022-07-08T19:44:38.193495Z", "status" => "active", "executedby" => "leo"},
  %{"name" => "Exportar Estado de Cuenta", "ini" => "2022-07-08T19:44:38.193495Z", "fin" => "2022-07-08T19:44:38.193495Z", "status" => "interrupted", "executedby" => "leo"},
  %{"name" => "Importar Cuenta", "ini" => "2022-07-08T19:44:38.193495Z", "fin" => "2022-07-08T19:44:38.193495Z", "status" => "paused", "executedby" => "leo"},
  %{"name" => "Modificar Cuenta", "ini" => "2022-07-08T19:44:38.193495Z", "fin" => "2022-07-08T19:44:38.193495Z", "status" => "closed", "executedby" => "leo"}
]
```

* Usando nombre colores de tailwind para los estados

```html
  <Timeline key="status">
    <Status name="active" color="indigo-600" description="Pendiente" svg="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
    <Status name="interrupted" color="yellow-600" description="Anulada" svg="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
    <Status name="paused" color="green-600" description="Detenida" svg="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
    <Status name="closed" color="red-600" description="Finalizada" svg="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
    <Rows records={record <- @records}>
      <Title>{record["name"]}</Title>
      <Item label="Completada por:">{record["executedby"]}</Item>
      <Item label="Fecha inicio:">{record["ini"]}</Item>
      <Item label="Fecha fin:" key="fin">{record["fin"]}</Item>
    </Rows>
  </Timeline>
```

* Usando nombre colores hex para los estados

```html
  <Timeline key="status">
    <Status name="active" color="#0070c0" description="Pendiente" svg="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
    <Status name="interrupted" color="#e53f40" description="Anulada" svg="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
    <Status name="paused" color="#ff9900" description="Detenida" svg="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
    <Status name="closed" color="#4ead6e" description="Finalizada" svg="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
    <Rows records={record <- @records}>
      <Title>{record["name"]}</Title>
      <Item label="Completada por:">{record["executedby"]}</Item>
      <Item label="Fecha inicio:">{record["ini"]}</Item>
      <Item label="Fecha fin:" key="fin">{record["fin"]}</Item>
    </Rows>
  </Timeline>
```

* Usando componente interno para las fechas

```html
  <Timeline key="status">
    <Status name="active" color="#0070c0" description="Pendiente" svg="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
    <Status name="interrupted" color="#e53f40" description="Anulada" svg="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
    <Status name="paused" color="#ff9900" description="Detenida" svg="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
    <Status name="closed" color="#4ead6e" description="Finalizada" svg="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
    <Rows records={record <- @records}>
      <Title>{record["name"]}</Title>
      <Item label="Completada por:">{record["executedby"]}</Item>
      <Item label="Fecha inicio:">
        <Date value={record["ini"]} />
      </Item>
      <Item label="Fecha fin:" key="fin">
        <Date value={record["fin"]} />
      </Item>
    </Rows>
  </Timeline>
```

* `key` Se usa para cargar las propiedades de los estados

### PhoenixComponents.Timeline.Status

* `name` Nombre de los estados que trae el payload
* `description` Descripción del estado
* `color` Color de fondo para el estado
* `svg` Ruta de dibujo, referencia el atributo `d` del path del svg

### PhoenixComponents.Timeline.Rows

* `records` Listado de elementos

#### PhoenixComponents.Timeline.Rows.Title

* `class` Clase para el titulo, por defecto: `"text-sm leading-6 font-medium text-gray-900 px-1"`

#### PhoenixComponents.Timeline.Rows.Item

* `label` Descripcion
* `key` Se usa para mostrar/ocultar el elemento en caso de estar vacío, el valor es el campo del payload

### PhoenixComponents.Timeline.Date

* `value` Se usa para formatear la fecha
* `tz_offset` Se usa para calcular la zona horaria, por defecto: `0`
