## PhoenixComponents.RoutePatch

### Integración en la vista tipo .sface

```html
<PhoenixComponents.RoutePatch
  route="{SkeletonWeb.Nav.Home}"
  class="inline-flex py-2 px-2 mr-4"
>
  <PhoenixComponents.Svg name="totalcheck" />
</PhoenixComponents.RoutePatch>

<PhoenixComponents.RoutePatch label="Inicio" route={SkeletonWeb.Nav.Home} />
```

* `label` en caso de no especificar slot
* `route` campo requerido para el enlace
* `class` campo opcional para clases css
* `slot` en caso de no especificar label

###  Integración en la vista tipo .html.heex

* import PhoenixComponents.RoutePatch

```html
    <.route_patch label="Volver" route={SkeletonWeb.Nav.Home} conn={@conn} />
```
