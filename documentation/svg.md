## PhoenixComponents.Svg

* Cargar imágenes desde repo svglobal.

```html
  <PhoenixComponents.Svg type="external" name="close" />
```

* Cargar imágenes desde el repo(proyecto) local.

```html
  <PhoenixComponents.Svg type="local" name="close" />
```

* Cargar imágenes desde url(no importa formato).

```html
  <PhoenixComponents.Svg type="url" name="/accounts/apps/totalcheck/backofficedigital.png" />
```

* Cargar imágenes desde alberto(no importa formato).

```html
  <PhoenixComponents.Svg type="uniqueid" name="fdd6c93c-0310-11ed-a09f-d2d27730a1a4" />
```

* `name` campo requerido, [nombre del fichero svg](https://bitbucket.org/teamdox/svglobal/src/master/priv/static/svg)
* `type` campo opcional, por defecto carga imagenes del repo `svglobal`. En caso de usar imagenes locales, colocarlas en `assets/static/svg/`
* `class` campo opcional
