## PhoenixComponents.Tabs
* Integración en la vista.
```html
    <PhoenixComponents.Tabs />
```

* Propiedad `line` Color de la linea cuando usa tab horizontal, por defecto es `gray-300`.
* Propiedad `vertical` Horientación de los tabs, por defecto es `false`.
* Propiedad `primary` Color primario para texto y borde del tab, por defecto es `indigo-500`.
* Propiedad `class` Clases para los tabs, por defecto es `border-transparent whitespace-nowrap flex py-2 px-1 border-b-2 font-medium text-sm`.

## PhoenixComponents.Tabs.TabItem
* Integración en la vista.
```html
  <PhoenixComponents.Tabs>
    <PhoenixComponents.Tabs.TabItem label="Detalles">
      Detalles
    </PhoenixComponents.Tabs.TabItem>
    <PhoenixComponents.Tabs.TabItem label="Historial">
      Historial de estados
    </PhoenixComponents.Tabs.TabItem>
  </PhoenixComponents.Tabs>
```

* Propiedad `label` Título del tabitem, por defecto es `""`
* Propiedad `class` Clase para cada tabitem, por defecto es `[]`
* Propiedad `hidden` Ocultar tabitem, por defecto es `false`