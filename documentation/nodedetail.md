## PhoenixComponents.NodeDetail

* Proceso LiveView.
```elixir
alias PhoenixComponents.{
  Preview,
  Preview.Wrapper,
  NodeDetail,
  NodeDetail.Detail,
  NodeDetail.Detail.Actions,
  Node
}

@impl true
  def mount(_params, session, socket) do
    assign(socket, :url, session["host"])
  end
```

* Integración de detalles en la vista.
```html
    <NodeDetail node={n}>
      <Detail>
        <Actions>
          <button title="Cerrar" type="button" class="inline-flex justify-center py-2 px-2 border border-transparent rounded-md text-white bg-primary-200 text-sm leading-5 font-medium hover:bg-opacity-90">
            <PhoenixComponents.RoutePatch route={SkeletonWeb.Nav.Home} opts={%{page: assigns["page"], per_page: assigns["per_page"], filter: assigns["filter"]} |> PhoenixComponents.clean_map}>
                <PhoenixComponents.Svg name="close" />
            </PhoenixComponents.RoutePatch>
          </button>
        </Actions>
        <Detail.Header>
          <h3 class="uppercase text-sm leading-6 font-medium text-gray-900">
            Detalles de la Solicitud
          </h3>
        </Detail.Header>
        <div class="overflow-hidden"> Información del nodo </div>
      </Detail>
    </NodeDetail>
```

* Propiedad `node` mapa tipo `%{selected: selected, children: children.records}`.
* Slot `Preview` Contenedor de previsualización de documentos.
* Slot `Detail` Contenedor de detalles

* Ejemplo usando `Preview y Node`.
```html
    <NodeDetail node={n}>
      <Preview>
        <Wrapper {=@url} node={n} id="preview" title={"Detalles de la solicitud #{n.node.data["nro_solicitud"]}"}>
          <Wrapper.Header>
            <div class="grid grid-cols-2 items-center">
              <h3 class="uppercase text-sm leading-6 font-medium text-gray-900">
                Documentos de la Solicitud ({n.children |> Enum.count})
              </h3>
              <span title="Recargar documento" class="flex items-center justify-end space-x-2 cursor-pointer" :on-click="refreshdoc" :values={id: "iframe.fullscreen"}>
                <PhoenixComponents.Svg name="refresh" />
              </span>
            </div>
          </Wrapper.Header>
        </Wrapper>
      </Preview>
      <Detail>
        <Actions>
          <button title="Cerrar" type="button" class="inline-flex justify-center py-2 px-2 border border-transparent rounded-md text-white bg-primary-200 text-sm leading-5 font-medium hover:bg-opacity-90">
            <PhoenixComponents.RoutePatch route={SkeletonWeb.Nav.Home} opts={%{page: assigns["page"], per_page: assigns["per_page"], filter: assigns["filter"]} |> PhoenixComponents.clean_map}>
                <PhoenixComponents.Svg name="close" />
            </PhoenixComponents.RoutePatch>
          </button>
        </Actions>
        <Detail.Header>
          <h3 class="uppercase text-sm leading-6 font-medium text-gray-900">
            Detalles de la Solicitud
          </h3>
        </Detail.Header>
        <div class="overflow-hidden">
          <SkeletonWeb.Components.CustomNode node={n.node} config={config} />
        </div>
      </Detail>
    </NodeDetail>
```

* Component local de ejemplo [CustomNode](https://bitbucket.org/teamdox/skeleton/src/master/lib/skeleton_web/components/custom_node/)
  * Propiedad `node` datos del expediente o nodo.
  * Propiedad `config` estructura en que se muestra la info, ejemplo:
  ```elixir
    defp config, do:
      %{
        "translate" => %{
          "inserted_at" => "Fecha creación",
          "creator" => "Nombre del solicitante",
          "nro_solicitud" => "Número de solicitud",
          "adquirientes" => "Datos del cliente",
          "cond_pago" => "Forma de pago",
          "direccion_completa" => "Dirección completa",
          "fecha_personeria" => "Fecha escritura pública",
          "ciudad_personeria" => "Ciudad personería",
          "notario_personeria" => "Notario personería",
          "datos_del_vehiculo" => "Datos del Vehículo",
          "ocupacion" => "Ocupación",
          "numero_motor" => "Número motor",
          "tipo_vehiculo" => "Tipo vehículo",
          "anual" => "Año",
          "chassis" => "Chasis",
          "telefono" => "Teléfono"
        },
        "key_persons" => [["data", "datos_del_cliente"]],
        "key_new" => %{
          "put" => ["data"],
          "fields" => %{
            "precio" => %{
              "parent" => ["data", "documento"],
              "children" => ["monto_total", "monto_exento"]
            },
            "datos_del_vehiculo" => %{
              "parent" => ["data", "vehiculo"],
              "children" => ["patente", "anual", "color", "marca", "modelo", "numero_motor", "tipo_vehiculo", "chassis"]
            },
            "datos_del_cliente" => %{
              "parent" => ["data", "adquirientes"],
              "children" => ["nombre_completo", "rut", "giro", "direccion_completa", "comuna", "ciudad"]
            },
            "solicitud" => [["data", "tipo_servicio"], ["data", "nro_solicitud"], ["inserted_at"], ["creator"]]
          }
        },
        "take_group" => [[["data"], "solicitud", "precio", "datos_del_cliente", "datos_del_vehiculo"]],
        "sort_group" => ["solicitud", "datos_del_vehiculo", "datos_del_cliente", "precio"],
        "delete_by_keys" => ["nombre","primer_apellido"]
      }
  ```