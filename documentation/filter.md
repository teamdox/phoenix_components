## PhoenixComponents.Filter

### Configuracion
En tu proceso LiveView colocas lo siguiente:

* `alias PhoenixComponents.Filter`

Ejemplo de como manejar los parametros en la URL. Tenga en cuenta que esto solo toma los parametros correspondientes al Filtro y actualiza el formulario.

* Eg. del `handle_params`:
  ```elixir
    @impl true
    def handle_params(params, _url, socket) do
      {
        :noreply,
        socket
        |> assign(:query_params, params)
        |> Filter.handle_params(params, "id_filter") # "id_filter" es el ID del filtro que vas a renderizar
      }
    end
  ```

**Integración en la vista para un Filter Simple.**

Desde el fichero .sface del proceso LiveView

* Eg. agregando un boton personalizado para abrir el filtro:
```html
  <Filter
  id="id_filter"
  title="New Project"
  description="Get started by filling in the information below to create your new project."
  {=@query_params}
  route={__MODULE__}
  action_class={"bg-primary-200"}
  >
    <:action>
      <button @click="open = !open" type="button" class="inline-flex py-2 px-4 text-sm leading-5 font-medium rounded-md text-white bg-primary-200 hover:bg-primary-100 active:bg-primary-100 transition duration-150 ease-in-out"> Filtrar </button>
    </:action>
    # Definicion de los campos del formulario.
    <Filter.Section label="Solicitud">
      <Filter.Item visible={funcion_o_dato_que_determina_visibilidad()} label="Número Solicitud" name="nro_solicitud" opts={[placeholder: "Ingrese número solicitud"]} />
      <Filter.Item type={:dropdown} label="Servicio" name="tipo_servicio" opts={[options: Utils.service_list]} />
      <Filter.Item type={:dropdown} label="Estado" name="estado" opts={[options: Utils.status_list]} />
      <Filter.Item type={:date} label="Fecha" name="inserted_at" opts={[placeholder: "Selecciona Rango", tz_offset: @tz_offset, single_picker?: false, custom_range?: true]}/>
    </Filter.Section>
    <Filter.Section label="Adquiriente">
      <Filter.Item label="Rut" name="rut_adquiriente" opts={[placeholder: "Ingrese RUT"]}/>
      <Filter.Item label="Nombre" name="nombre_completo_adquiriente" opts={[placeholder: "Ingrese Nombre"]}/>
    </Filter.Section>  
    <Filter.Section label="Vehículo">
      <Filter.Item label="PPU" name="patente" opts={[placeholder: "Ingrese Patente"]}/>
    </Filter.Section>
  </Filter>
```

#### Etiqueta **PhoenixComponents.Filter**
* `id` Id del filtro
* `accordion` Comportamiento de accordion
* `bordered` Agrega bordes a secciones y elementos de la misma
* `title` Título en la cabecera del filtro
* `description` Descripción en la cabecera del filtro
* `query_params` Parámetros de la url, variable creada en handle_params
* `route` Ruta o módulo donde se vaya a usar el componente
* `action_class` Clase primaria para el boton, por defecto tiene `bg-blue-600`
* `header_class` Clase para cabecera, por defecto tiene `bg-blue-600 py-6 px-4 sm:px-6`
* `left` Posicionarlo a la izquierda, por defecto el panel sale a la derecha

#### Etiqueta **PhoenixComponents.Filter.Section**
* `class` Clase de la seccion, por defecto tiene `w-full font-base leading-6 uppercase`
* `opened` Propiedad para comportamiento de accordion, por defecto tiene `false`
* `label` Etiqueta para definir texto en la seccion

#### Etiqueta **PhoenixComponents.Filter.Item**
* `name` nombre del campo en el formulario(no es necesario definirlo para los campos de tipo `:section`.)
* `type` atom que representa el tipo del campo. Hasta el momento solo acepta:
   `:text` para campos de tipo texto. (Valor por defecto)
   `:select` para campos de selección única
   `:dropdown` para campos de selección múltiple.
   `:date` para campos de tipo fecha (Rangos o selección Simple).
* `label` Texto correspondiente a cada campo. Para el tipo `:section` se corresponde con el título a mostrar en la sección.
* `value` valor que tomará el campo. (no es necesario definirlo para los campos de tipo `:section`.)
* `visible` determina si deseas mostrar o no el campo dentro del formulario. Por defecto tiene valor `true`
* `opts` opciones que se le pasa al campo. Eg.(`class:`, `options:`, `placeholder`). No es obligatorio definirlo. En el caso de componentes creados por nuestro equipo, puede consultar mas opciones en el README de cada componente.


* Eg. usando header por defecto:
```html
  <PhoenixComponents.Filter
  id="filtro1" #ID del Filtro
  title="New Project" # Titulo del Filtro
  label="Filtrar" # Texto del boton que abre el panel (Opcional)
  description="Get started by filling in the information below to create your new project." # Descripcion
  {=@query_params} # Parametros en la url, variable creada en handle_params
  route={__MODULE__} # Ruta del modulo
  action_class="bg-primary-200" # clase primaria para el btn
  >
    # A continuacion se debe definir el listado de Campos en el formulario
    <PhoenixComponents.Filter.Section />
  </PhoenixComponents.Filter>
```

* Eg. usando un header personalizado:
```elixir
  <PhoenixComponents.Filter
  id="filtro1" #ID del Filtro
  label="Filtrar" # Texto del boton que abre el panel
  {=@query_params} # Parametros en la url, variable creada en handle_params
  route={__MODULE__} # Ruta del modulo
  action_class="bg-primary-200" # clase primaria para el btn
  >
    <PhoenixComponents.Filter.Header>
      # HTML personalizado. Nota que no tienes que definir el title y la description en las propiedades del Componente.
    </PhoenixComponents.Filter.Header>
    # A continuacion se debe definir el listado de Campos en el formulario
    <PhoenixComponents.Filter.Section />
  </PhoenixComponents.Filter>
```
* Para header personalizado, la acción cerrar panel, debe agregar la función `@click="open = !open"`
