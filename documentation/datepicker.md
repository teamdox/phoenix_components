## PhoenixComponents.DatePicker

El componente **DatePicker** está diseñado para poder seleccionar una fecha o un rango de fecha. Permite la navegación del calendario por meses y por años. Tiene una navegación de 200 años, tomando como punto de partida la fecha actual(100 años <= hoy >= 100 años). Se utiliza Tailwind para el estilo del componente y Timex para las funciones de fecha.

### Dependencias
```elixir
defp deps do
  [
    {:timex, "~> 3.7.6"}
  ]
```

**Integración en la vista para un DatePicker Simple.**

```elixir
  <PhoenixComponents.DatePicker
    id= "id_component"
    name="inserted_at"
    value={@inserted_at}
    placeholder="Fecha de inicio"
    tz_offset={-4}
    sufix_class="primary-200"
    focused_class="border-2 outline-none ring-indigo-500 border-indigo-500"
  />
```
**Integración en la vista para un DatePicker de Rango.**

```elixir
  <PhoenixComponents.DatePicker
    id= "id_component"
    name="inserted_at"
    value={@inserted_at}
    placeholder="Seleccione un rango"
    tz_offset={-4}
    sufix_class="primary-200"
    sufix_hover_class="primary-300"
    single_picker?={false}
    custom_range?= {true}
    ranges= {[:today, :yesterday, %{label: "Ultimos 5 años", amount: -5, in: :years}]}
    focused_class="border-2 outline-none ring-indigo-500 border-indigo-500"
  />
```
**Integracion con el Componente PhoenixComponents.Filter**

```elixir
  <PhoenixComponents.Filter>
    ..
    <PhoenixComponents.Filter.Item
      type={:date}
      label="Fecha"
      name="inserted_at"
      opts={[
        placeholder: "Seleccione un Rango",
        tz_offset: @tz_offset,
        single_picker?: false,
        custom_range?: true,
        ranges: [:today, :yesterday, %{label: "Ultimos 5 años", amount: -5, in: :years}],
        sufix_class: "primary-200",
        sufix_hover_class: "primary-300"
        focused_class: "border-2 outline-none ring-indigo-500 border-indigo-500"
        ]}/>
  </PhoenixComponents.Filter>
```

* `id` Especificar un id diferente en cada lugar que se use el componente.
* `name` Nombre del input en HTML.
* `value` Se corresponde con el valor que se muestra en el input.
* `placeholder` Especificar un placeholder al elemento input. Por defecto tiene el valor `Seleccione`
* `tz_offset` Numero que identifica el `time_zone_offset` del navegador, por defecto es 0
* `sufix_class` Variante Tailwind para cambiar el background de las selecciones en el componente.
* `sufix_hover_class` Solo es necesario cuando el DatePicker es de Rango. Se utiliza para cambiar la clase `hover` de los elementos que están dentro del rango que se selecciona y en las opciones de los rangos predeterminados.
* `single_picker?` Especifica el tipo de DatePicker (`true` -> **Simple**, `false` -> **Rango**). Por defecto es `true`.
* `custom_range?` Este valor es solo tomado en cuenta si `single_picker?` tiene valor `false`. En dependencia de su valor(`true` o `false`), se agrega al listado de rangos la opcion para seleccionar un rango personalizado. Por defecto tiene valor `false`
* `ranges` Se espera que sea un listado de rangos. De no especificar ningún valor, el componente toma los rangos por defectos para darle la opción de que tenga rangos personalizados. A continuación se detalla como definir los rangos personalizados.
* `focused_class` Define clases para el foco que tendrá el campo de fecha.


### Rangos Personalizados
#### Por defecto
El componente tiene los siguientes Rangos predefinidos y que usted puede usar sin tener que especificarlo en la configuración del mismo.
```elixir
today: "Hoy",
yesterday: "Ayer",
last_7days: "Últimos 7 días", # Se incluye el dia de hoy como parte del rango
last_30days: "Últimos 30 días", # Se incluye el dia de hoy como parte del rango
this_month: "Mes actual",
last_month: "Mes pasado",
this_year: "Año actual"
```
#### Personalizar rango
Se puede hacer de dos manera.
* Si deseas usar uno definido por el componente solo debes especificar una **key** de las antes mencionadas.
Eg.
```elixir
<PhoenixComponents.DatePicker
    id= "id_component"
    ..
    ranges={[:today, :yesterday]}
 />
```
* Puedes definir uno completamente nuevo, para ello defines un mapa con la siguiente estructura.
`%{label: label, amount: amount, in: step}`
se espera que `label` es una cadena, `amount` un entero, `in` un atom (`:days`, `:months`, `:years`).
Eg.
```elixir
<PhoenixComponents.DatePicker
    id= "id_component"
    ..
    ranges={[
      :today, # Puedes combinar ambas configuraciones.
      %{label: "Mes próximo", amount: 1, in: :months},
      %{label: "Ultimos 5 años", amount: -5, in: :years}
    ]}
 />
```
### Como determinar el tz_offset
En el archivo ``app.js`` cuando se define el **LiveSocket**
```js
let liveSocket = new LiveSocket("/app/live", Socket, {
	hooks: Hooks,
	params: {
		_csrf_token: csrfToken,
		tz_offset: -(new Date().getTimezoneOffset()/60) //Calcular el timezone_offset y pasarlo como parámetro de conexión.
	}})
```
Luego en el proceso LiveView, en el mount puedes captar este dato usando la siguiente función
`get_connect_params(socket)`
```elixir
@impl true
  def mount(_params, session, socket) do
  tz_offset =
    socket
    |> get_connect_params # Funcion de Phoenix que devuelve los parametros de conexion.
    |> get_time_zone_offset() # Extraemos el tz_offset que definimos app.js
  {:ok, socket |> assign(tz_offset: tz_offset)}
  end

  defp get_time_zone_offset(%{"tz_offset" => tz_offset}), do: tz_offset # Buscar por la misma key que se definio en app.js
  defp get_time_zone_offset(_), do: 0
```
