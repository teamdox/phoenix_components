## PhoenixComponents.Tabs
* LLamando alias en el módulo

```elixir
  alias PhoenixComponents.{
    Modal,
    Modal.Open,
    Modal.Header,
    Modal.Content,
    Modal.Footer
  }

  # Uso general
  def handle_event("open_modal", %{"id" => id}, socket) do
    Modal.set_action(id, true)
    {:noreply, socket}
  end
```

* Propiedad `id` Identificador del modal.
* Propiedad `width` Tamaño del modal, por defecto es `xl` - esta propiedad hace referencia a `max-w-` de tailwind.
* Evento `open_modal` Evento personalizado que abre el Modal, por defecto toma valor `open_modal`.
* Slot `open` Contenido para abrir el modal, es opcional.
* Slot `header` Contenido para la cabecera del modal.
* Slot `content` Contenido del modal.
* Slot `footer` Contenido para los botones del modal.

* Integración simple en la vista.
```html
  <Modal id="modal">
    <Open label="Abrir" />
    <Header> header </Header>
    <Content> contenido </Content>
    <Footer> botones </Footer>
  </Modal>
```

* Integración pasándole un tamaño en la vista.
```html
  <Modal id="modal" width="2xl">
    <Open label="Abrir" />
    <Header> header </Header>
    <Content> contenido </Content>
    <Footer> botones </Footer>
  </Modal>
```

* Usando un btn abrir modal fuera del componente.
```html
  <button type="button" :on-click="open_modal" :values={id: "modal"}> Cancelar </button>
  <Modal id="modal" width="2xl">
    <Header> header </Header>
    <Content> contenido </Content>
    <Footer> botones </Footer>
  </Modal>
```