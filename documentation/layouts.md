## Layouts
### PhoenixComponents.MainLayout
### Definiendo alias
```elixir
  alias PhoenixComponents.{
    MiddleMenubar,
    MainLayout,
    MainLayout.Navbar,
    MainLayout.Header,
    MainLayout.Main
  }
```

### Integración en la vista.
```html
  <MainLayout class="px-6" relative>
    <Header class="px-6">
      <Header.Left> Logos </Header.Left>
      <Header.Right> Switch aplicaciones y menu de perfil </Header.Right>
    </Header>
    <Navbar class="px-6 bg-white shadow"> Barra de menu </Navbar>
    {@inner_content}
    <Footer class="px-6"> Pie de página </Footer>
  </MainLayout>
```
* `relative` Atributo booleano que coloca clase relative a la etiqueta HTML `<main>`. Por defecto es `false`

### Integración en la vista usando middle_menubar.
```html
  <MainLayout class="px-6">
    <Header class="px-6">
      <Header.Left> Logos </Header.Left>
      <Header.Right> Switch aplicaciones y menu de perfil </Header.Right>
    </Header>
    <Navbar class="px-6 bg-white shadow">
      <MiddleMenubar>
        <MiddleMenubar.Left> Contenido menu izquierdo </MiddleMenubar.Left>
        <MiddleMenubar.Right> Contenido menu derecho </MiddleMenubar.Right>
      </MiddleMenubar>
    </Navbar>
    {@inner_content}
    <Footer class="px-6"> Pie de página </Footer>
  </MainLayout>
```

### PhoenixComponents.SimpleLayout
```html
  <SimpleLayout>
    <Header>
        <Left> Logo y nombre de la app </Left>
        <Navbar> Barra de menu </Navbar>
        <Right>  Switch aplicaciones </Right>
        <Profile> Menu de perfil </Profile>
    </Header>
    {@inner_content}
  </SimpleLayout>
```
