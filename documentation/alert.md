## PhoenixComponents.Alert
* Es necesario agregar en assets/tailwind.config.js:
```js
	theme: {
			colors: {
				primary: {
					'components': '#0a568a' // Queda a cargo de cada equipo elegir el color primerio de los componentes
				},
				alert: { // Colores para los iconos y textos del Alert
					'question': '#4dc0b5', //color Icono de pregunta
					'info': '#4dc0b5', //color Icono de info
					'error': '#FF0000', //color Icono de error
					'cancelled': '#FF0000', //color Icono de cancelled
					'warn': '#FFA136', //color Icono de warn
					'succes': '#38C172' //color Icono de succes
				}
      }
  }
```

* Integración en la vista.
```html
    <PhoenixComponents.Alert
      id={@modal_id}
      type={:question}
      ok_label="Confirmar"
      close_label="Cancelar"
      ok_event="evento_de_confirmacion"
      data={[id: @modal_id, meta: "Otra info"]}
      title="¿Confirma el ingreso?"
      description="La solicitud quedará como ingresada y avanzará la tarea."
      >
      <PhoenixComponents.Alert.Open class="p-4">
          <PhoenixComponents.Svg name="svg_name"/>
      </PhoenixComponents.Alert.Open>
      <PhoenixComponents.Alert.Actions>
        <button type="button" :on-click="accion1">
          Accion1
        </button>
      </PhoenixComponents.Alert.Actions>
    </PhoenixComponents.Alert>
```

* Propiedad `width` Tamaño del dialogo, por defecto es `lg` - se corresponde con clase tailwind
* Propiedad `title` Título del Alert, este aparece debajo del Icono, requerido.
* Propiedad `description` Descripcion del Alert.
* Propiedad `type` Tipo de Alerta. Posibles valores `:info`, `:warn`, `:error`, `:succes`, `:cancelled`, `:question`. Por defecto esta en `:info`.
* Propiedad `ok_label` Texto del boton Ok, por defecto es Aceptar.
* Propiedad `close_label` Texto del boton Cancelar, por defecto es Cancelar.
* Propiedad `data` Keyword que contiene datos que se desean enviar al hacer ok.
* Propiedad `uppercase` Si desea que los botones salgan en Mayuscula, por defecto es false.
* Evento `ok_event` Evento personalizado al hacer Ok. Por defecto, cierra el Dialogo.
* Evento `open_event` Evento personalizado que abre el Dialogo, No es necesario definirlo.
* Slot `open` Slot para agregar cualquier contenido HTML para abrir el Dialogo. Es requerido
* Slot `default` En caso de no tener una `description` para la Alerta, pues agregar contenido HTML al cuerpo de la misma.
* Slot `actions` Si desea reemplazar el boton de OK por otro contenido HTML

### NOTA:
Por reglas de diseño el boton cancelar solo aparece cuando es una Alerta de tipo `:question`
