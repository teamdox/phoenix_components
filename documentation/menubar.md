## Menubar
### PhoenixComponents.MiddleMenubar
### Definiendo alias
```elixir
  alias PhoenixComponents.{
    MiddleMenubar
  }
```

### Integración en la vista
```html
  <MiddleMenubar>
    <MiddleMenubar.Left> Enlaces de navegación </MiddleMenubar.Left>
    <MiddleMenubar.Right> Iconos </MiddleMenubar.Right>
  </MiddleMenubar>
```