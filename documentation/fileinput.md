## PhoenixComponents.FileInput

### Configuracion

Ejemplo de como manejar obtener los documentos

  ```elixir
    data record, :any, default: %{}

    def handle_event("changes", params, socket) do
      params["record"]["documento"] |> Jason.decode! |> IO.inspect(limit: :infinity)
      {:noreply, socket}
    end
  ```

**Integración en la vista**

Desde el fichero .sface del proceso LiveView

```html
    <Surface.Components.Form
      for={:record}
      submit="changes"
      opts={autocomplete: "off"}
      multipart={true}
      class="w-full"
    >
    <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-gray-200 sm:border-t sm:pt-5">
      <Surface.Components.Form.Label class="block text-sm font-medium text-gray-700">Documento</Surface.Components.Form.Label>
      <div class="mt-1 sm:mt-0 sm:col-span-2">
        <PhoenixComponents.FileInput id="documento" name="record[documento]" multiple options={["Certificado de Anotaciones Vigentes": "cav"]} />
      </div>
    </div>
    <div class="pt-5">
      <div class="flex justify-end">
        <button type="submit" class="py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-primary-300">
          Submit
        </button>
      </div>
  </div>
  </Surface.Components.Form>
```

* `accept` tipo de documento a cargar
* `name` parámetro donde se guarda el documento
* `multiple` permitir carga múltiples
* `webkitdirectory` permitir cargar documentos de una carpeta en Chrome
* `mozdirectory` permitir cargar documentos de una carpeta en Mozilla
* `class_select` clase para el select del listado de tipos documentales
* `class_group` clase para el contenedor de documentos cargados
* `label` texto del select
* `options` listado de elementos a mostrar en el select. Ej: `["Certificado de Anotaciones Vigentes": "cav"]`