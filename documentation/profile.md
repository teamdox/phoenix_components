## PhoenixComponents.Profile

* Integración en la vista.
```html
  <PhoenixComponents.Profile ticket={ticket} redirect={redirect} id="profile" />
```
* `ticket` ticket del usuario autenticado
* `redirect` string usado para redireccionar al usuario cuando se cierre la sesión
