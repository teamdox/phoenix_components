## PhoenixComponents.Table.Simple

Agregar lo siguiente en el modulo:

* `alias PhoenixComponents.Table.{Simple, Column}`
* Eg. del `handle_params`:

  ```elixir
    @impl true
    def handle_params(params, _url, socket) do
      page = params["page"] || 1
      perpage = params["per_page"] || 10

      data =
        Expedientes.search_m(page, perpage, "solicitud_servicio")
        |> Expedientes.clean_monad_records()

      {:noreply,
        socket
        |> assign(:query_params, params)
        |> assign(data)
        |> Simple.handle_params(params)
      }
    end
  ```

* Integración en la vista.

```html
  <Simple id="table" records={row <- @records} {=@page} {=@per_page} {=@total_page} {=@total_records} {=@query_params} route={__MODULE__}>
    <Column label="Número Solicitud">
      {row["nro_solicitud"]}
    </Column>
  </Simple>
```

* `id` especificar un id diferente en cada lugar que se use el componente
* `page` pagina actual
* `per_page` elementos por pagina
* `total_records` total de registros
* `total_page` total de paginas
* `options_select` listado de elementos por pagina, por defecto es `[5, 10, 15, 20, 100]`
* `route` ruta de la pagina actual
* `align` alineación de las filas, posibles valores `text-left text-right text-center justify-start justify-end justify-center`, por defecto `text-left`

### Column

* `label` titulo de la columna
* `align` alineación de las columas, posibles valores `text-left text-right text-center justify-start justify-end justify-center`, por defecto toma valor de alineación de las filas, en caso de no tener uno toma `text-left`

### Modulos usados para expedientes y búsqueda en alberto

* [Expedientes](https://bitbucket.org/teamdox/servicio_solicitud/src/development/lib/solicitudservicios/expedientes.ex)

