## PhoenixComponents.Logos

* Simple

```html
  <PhoenixComponents.Logos>
    <PhoenixComponents.Logos.Item tenant="santander" class="h-10" />
  </PhoenixComponents.Logos>
```

* Combinando con imagenes externas

```html
  <PhoenixComponents.Logos>
    <PhoenixComponents.Logos.Item tenant="santander" class="h-10" />
    <PhoenixComponents.Logos.Item tenant="dercomaq" type="external" class="h-10" />
  </PhoenixComponents.Logos>
```

* Avanzada

```html
  <PhoenixComponents.Logos>
    <PhoenixComponents.Logos.Item tenant="totalcheckautomotriz" class="h-10" route={SolicitudserviciosWeb.Nav.Home} />
    <PhoenixComponents.Logos.Item tenant="santander" class="h-10" />
    <PhoenixComponents.Logos.Item name="dercomaq" type="external" class="h-10" />
  </PhoenixComponents.Logos>
```

* `index` Indice de búsqueda, por defecto `content`
* `class` Clase, por defecto `inline-flex items-center space-x-4 py-2`

### PhoenixComponents.Logos.Item

* `tenant` Tenant con el que se subió a alberto la imagen
* `name` Nombre de la imagen sin la extensión, por defecto se usa `logo`
* `class` Clase, por defecto `[]`
* `type` Tipo de imagen a cargar, por defecto `uniqueid`. Opciones `external, local, url, uniqueid`
* `route` Ruta
