## PhoenixComponents.FlyoutMenus

### Definiendo alias
```elixir
  alias PhoenixComponents.{
    FlyoutMenus,
    FlyoutMenus.Link
  }
```

* Usando Route atom

```html
  <FlyoutMenus selected="ingreso_rpsd" label="RPSD">
    <Link route={Nav.Home} tag="ingreso_rpsd">Ingreso</Link>
    <Link route={Nav.Detenidos} tag="detenido_rpsd">Detenidos</Link>
    <Link route={Nav.RegistrarPago} tag="registrar_pago">Registro de Pago</Link>
  </FlyoutMenus>
```

* Usando route string

```html
  <FlyoutMenus selected="ingreso_rpsd" label="RPSD">
    <Link route="/alzamientoprenda" tag="ingreso_rpsd">Ingreso</Link>
    <Link route="/alzamientoprenda/detenidos" tag="detenido_rpsd">Detenidos</Link>
    <Link route="/alzamientoprenda/pagos" tag="registrar_pago">Registro de Pago</Link>
  </FlyoutMenus>
```

* Propiedad `activeClass` Para cuando el menu esta activo, por defecto: `~w(border-b-4 text-gray-600 border-primary-200)`
* Propiedad `textMenuHover` Para hover texto del menu, por defecto: `"gray-600"`
* Propiedad `textMenu` Color texto del menu, por default: `"gray-400"`
* Propiedad `bgMenu` Color fondo del menu, por defecto: `"white"`
* Propiedad `selected` Elemento del menu seleccionado
* Propiedad `bgLinkHover` Hover del color fondo de cada enlace, por defecto: `"gray-100"`
* Propiedad `textLink` Color del texto de cada enlace
* Propiedad `textLinkHover` Hover del color texto de cada enlace
* Propiedad `textLinkActive` Color activo del texto de cada enlace, por defecto: `"primary-200"`
* Propiedad `linkWidth` Ancho de cada enlace, usando clase de tailwind `px-`, por defecto: `"4"`
* Propiedad `linkHeight` Alto de cada enlace, usando clase de tailwind `py-`, por defecto: `"2"`
* Propiedad `marginTop` Margen superior del menu, por defecto: `"2"`
* Propiedad `rounded` Bordes redondeados del menu, por defecto: `"md"`
* Propiedad `shadow` Sombra del menu, por defecto: `"lg"`
* Propiedad `label` Texto del menu, por defecto: `"Label"`
* Slot `title`
* Slot `links`
* Slot `default`

### PhoenixComponents.FlyoutMenus.Link

* Propiedad `tag`: determinar elemento seleccionado del menu 
* Propiedad `route`: enlace puede especificar `atom` o `string`
* Propiedad `whitespace`: separación del texto, por defecto: `"nowrap"`
* Propiedad `hidden`: ocultar enlace, por defecto: `false`
* Slot `default`

### PhoenixComponents.FlyoutMenus.Title

* Slot `default`
