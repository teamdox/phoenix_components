## PhoenixComponents.Filter.Badges

### Configuracion
Podrias definir el map para la traducción de parametros como parte del modelo o pasarsela directamente al componente.

* Eg. de definicion usando `surface`:
  ```elixir
  data keys, :map, default: %{
    "nro_solicitud" => "Número solicitud",
    "estado" => "Estado",
    "inserted_at" => "Fecha solicitud",
    "tipo_servicio" => "Servicio",
    "rut_adquiriente" => "Rut",
    "nombre_completo_adquiriente" => "Número solicitud",
    "patente" => "PPU"
  }
  ```

Ejemplo de como manejar los parametros en la URL. Se debe agregar `query_params` con los parametros.

* Eg. del `handle_params`:
  ```elixir
    @impl true
    def handle_params(params, _url, socket) do
      {
        :noreply,
        socket
        |> assign(:query_params, params)
      }
    end
  ```

**Integración en la vista.**

Desde el fichero .sface del proceso LiveView

* Eg. :
```html
  <PhoenixComponents.Filter.Badges
    id="badges"
    route={__MODULE__}
    keys={@keys}
    query_params={@query_params}
    filter_id={@filter_id}
    tz_offset={@tz_offset}
  />
```
#### Estructura 
* `route` Ruta o módulo donde se vaya a usar el componente
* `keys` Mapa que define la traducción de los parametros. Ej: `%{"nro_solicitud" => "Número de Solicitud"}`
* `query_params` Parámetros de la url
* `filter_id` Identificador del filtro
* `tz_offset` Time zone offset, Ej: `-4`, por defecto toma `0`