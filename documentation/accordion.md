## PhoenixComponents.Accordion
* Integración en la vista.
```html
    <PhoenixComponents.Accordion />
```

* Propiedad `collapse` Comportamiento del accordion, por defecto es `false`.
* Propiedad `rounded` Redondeado, por defecto es `false`.
* Propiedad `px` Padding a lo largo, por defecto es `3`.
* Propiedad `py` Padding a lo ancho, por defecto es `2`.
* Propiedad `iconColor` Color del icono, por defecto es `gray-600`.
* Propiedad `class` Clase para cada elemento, por defecto es `text-gray-600 font-medium`.

## PhoenixComponents.Accordion.Item
* Integración en la vista.
```html
  <PhoenixComponents.Accordion>
    <PhoenixComponents.Accordion.Item label="Detalles">
      Detalles
    </PhoenixComponents.Accordion.Item>
    <PhoenixComponents.Accordion.Item label="Historial">
      Historial de estados
    </PhoenixComponents.Accordion.Item>
  </PhoenixComponents.Accordion>
```

* Propiedad `label` Título del Item, por defecto es `""`