## PhoenixComponents.Dropdown

### Configuracion

Ejemplo de como manejar los parametros del formulario

* Eg. del `handle_params`:
  ```elixir
    # En drop se guarda la info seleccionada
    def handle_event("changes", %{"drop" => drop}, socket) do
      {:noreply, socket}
    end
  ```

**Integración en la vista**

Desde el fichero .sface del proceso LiveView

```html
  <Surface.Components.Form
    for={:drop}
    change="changes"
    opts={autocomplete: "off"}
    class="flex flex-col bg-white shadow-xl"
  >
    <PhoenixComponents.Dropdown form={:drop} name="tipo_servicio" selected={@drop[tipo_servicio] || []} options={service_list} id={"drop-#{field.name}-dropdown"} />
  </Surface.Components.Form>
```

* `form` nombre del formulario
* `name` nombre del campo
* `selected` lista de parametros seleccionados
* `id` identificador del componente
* `options` lista de optiones a mostrar:

* Eg. del `service_list`:
  ```elixir
  defp service_list, do:
    [ "Factura al Contado": "factura_al_contado",
      "Factura con Crédito": "factura_con_credito",
      "Dación de Vehículos": "dacion",
      "Adjudicación de Vehículos": "adjudicacion"
    ]
  ```
