## PhoenixComponents.Toggle

* Ejemplo simple
  
```html
  <div class="flex">
    <PhoenixComponents.Toggle id="toggle" name="name_form" options={{"option_true", "option_false"}} enabled />
    <span class="ml-2">Toggle</span>
  </div>
```

* Ejemplo avanzado

```html
  <div class="flex">
    <PhoenixComponents.Toggle
      id="toggle"
      name="detail[tipo_firma_mandato]"
      options={{"firma_electronica", "firma_manual"}}
      enabled={@detail["tipo_firma_mandato"] != "firma_manual"}
    />
    <span class="ml-2">Firma Electrónica?</span>
  </div>
```

* Propiedad `id` especificar un id diferente en cada lugar que se use el componente
* Propiedad `name` parámetro que se envia con el valor seleccionado cuando se hace submit
* Propiedad `options` tupla que contiene `{valor_activado, valor_desactivado}`
* Propiedad `enabled` valor para activar o desactivar toggle, por defecto toma `false`