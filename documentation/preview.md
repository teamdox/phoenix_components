## PhoenixComponents.Preview

* Proceso LiveView.
```elixir
alias PhoenixComponents.{
  Preview,
  Preview.Wrapper
}

@impl true
def mount(_params, session, socket) do
  socket
  |> assign(:url_base, session["host"])
end
```

* Integración en la vista.
```html
  <Preview>
    <Wrapper id="preview"
      reload
      node={n}
      {=@url_base}
      preview="/nodeservice/preview/"
      download="/nodeservice/download/"
      title="Detalles del expediente "
      >
      <Wrapper.Header>
        <h3 class="uppercase text-sm leading-6 font-medium text-gray-900">
          Documentos de la Solicitud ({n.children |> Enum.count})
        </h3>
      </Wrapper.Header>
    </Wrapper>
  </Preview>
```

* Slot `Wrapper`
  * Propiedad `id` especificar un id diferente en cada lugar que se use el componente
  * Propiedad `title` titulo en la cabecera
  * Propiedad `reload` incluye la opción de recarga del documento
  * Propiedad `node` se espera que sea un mapa `%{selected: selected, children: children.records}`
  * Propiedad `url_base` Host de la app
  * Propiedad `preview` Url de previsualizar
  * Propiedad `download` Url de descarga
  * Propiedad `translate` traducción de los tipos contenidos, se espera un keyword tipo `[type: "Descripción"]`
  * Propiedad `custom_preview` Url perzonalizada para previsualizar, eg: `"/docx-generator-service/document/download/"`
  * Function `set_action` Actualiza el previsualizador, eg: `Wrapper.set_action(id_componente, id_documento)`