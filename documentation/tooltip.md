## PhoenixComponents.Tooltip

* Integración en la vista.
```html
  <PhoenixComponents.Tooltip tip="Adjuntar" tip_class="translate-y-1 -translate-x-20">
    <PhoenixComponents.Svg name="attachment" class="w-5 h-5 text-preview-icon" />
  </PhoenixComponents.Tooltip>
```

* Propiedad `tip` Texto a mostrar.
* Propiedad `class` Clases tailwind aplicadas al contendor del componente.
* Propiedad `tip_class` Clases tailwind que se desean aplicar al tip. Ej: Trasladar(X-Y) el Tip a una posicion.
* Slot `default` Es requerido y en el se define el elemneto al cual se le quiere aplicar el Tip(SVG, texto).
