# PhoenixComponents

**TODO: Agregar descripcion**

## Default

```elixir
def deps do
  [
    {:phoenix_components, git: "https://bitbucket.org/teamdox/phoenix_components"}
  ]
end
```

## Documentación
* [DatePicker](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/datepicker.md)
* [Dropdown](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/dropdown.md)
* [Empty](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/empty.md)
* [Filter](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/filter.md)
* [Filter Badges](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/badges.md)
* [Layouts](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/layouts.md)
* [Menubar](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/menubar.md)
* [Navmenu](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/navmenu.md)
* [NodeDetail](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/nodedetail.md)
* [Paginate](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/paginate.md)
* [Preview](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/preview.md)
* [Profile](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/profile.md)
* [RoutePatch](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/routepatch.md)
* [Svg](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/svg.md)
* [Table](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/table.md)
* [FileInput](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/fileinput.md)
* [ToolTip](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/tooltip.md)
* [Alert](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/alert.md)
* [Tabs](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/tabs.md)
* [Modal](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/modal.md)
* [Toggle](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/toggle.md)
* [Logos](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/logos.md)
* [Timeline](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/timeline.md)
* [FlyoutMenus](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/flyoutmenus.md)
* [Accordion](https://bitbucket.org/teamdox/phoenix_components/src/master/documentation/accordion.md)

